import pickle
import matplotlib.pyplot as plt
import numpy as np
import json
import os


def compare_to_random(other_strategy, other_strategy_name, metric):
    upper_bounds = [i + j for i, j in zip(result[0][metric],variances[0][metric])]
    lower_bounds = [i - j for i, j in zip(result[0][metric], variances[0][metric])]
    win, loss, tie = 0, 0, 0
    for indx, val in enumerate(other_strategy):
        if val > upper_bounds[indx]:
            win+=1
        elif val < lower_bounds[indx]:
            loss-=1
        else:
            tie+=1
    output_dict = {'win':win, 'tie':tie, 'loss':loss}
    return output_dict.values(), max(output_dict, key=output_dict.get)

def output_latex_table(numpy_array):
    output_array = np.ndarray([numpy_array.shape[0]+1, numpy_array.shape[1]+1], dtype='object')
    arms = config_dict['arms']
    metrics = config_dict['metrics']
    for i in range(numpy_array.shape[0]):
        for j in range(numpy_array.shape[1]):
            output_array[i+1][0] = arms[i]
            output_array[0][j+1] = metrics[j]
            max_value = max(numpy_array[i][j])
            output_array[i+1][j+1] = str(numpy_array[i][j])[1:-1].replace(' ','/').replace(str(max_value), '\\textbf{%s}' % max_value)
    print(" \\\\\n".join([" & ".join(map(str, line)) for line in output_array]))

def plot_dataset_metric(dataset, metric):
    query_num = np.arange(1, quota + 1)
    print(len(query_num), len(result[1]['acc']))
    for i, name in enumerate(config_dict['arms']):
        plt.errorbar(query_num, result[i][metric], yerr=variances[i][metric], label=name)
    plt.xlabel('Number of Queries', fontsize=18)
    plt.ylabel('%s' % metric, fontsize=18)
    plt.title('%s against number of queries for %s' % (metric, dataset), fontsize=18)
    plt.legend(loc='best')
    plt.savefig(path_for_run + '/%s_%s.png' % (dataset, metric))
    plt.close()

PATH = os.getcwd()
ALC_data = ['hiva', 'ibn_sina', 'sylva', 'nova']
metrics = ['f1', 'acc', 'pre', 'rec', 'auc']
path_for_run = PATH + '/ALBL_UCB1_logs/26-08-16-16-07-28'
config_dict = json.load(open(path_for_run+'/config.json', 'r'))
table_output = np.array([[[0, 0, 0] for i in range(len(metrics))] for j in range(len(config_dict['arms']))])

for dataset in ALC_data:

    results = json.load(open(path_for_run + '/%s_results.json' % dataset, 'r'))
    quota = config_dict['n_labels']
    result = []
    variances = []

    for i in results:
        ith_dict_result = {}
        ith_dict_variances = {}
        for metric in metrics:
            ith_dict_result.update({metric: np.mean(i[metric], axis=0)})
            ith_dict_variances.update({metric: np.var(i[metric], axis=0)})
        result.append(ith_dict_result)
        variances.append(ith_dict_variances)

    for i, name in enumerate(config_dict['arms']):
        for j, metric in enumerate(metrics):
            output, win_loss_tie = compare_to_random(result[i][metric], name, metric)
            if win_loss_tie == 'win':
                table_output[i][j][0] += 1
            elif win_loss_tie == 'tie':
                table_output[i][j][1] += 1
            else:
                table_output[i][j][2] += 1

    plot_dataset_metric(dataset,'acc')
output_latex_table(table_output)

