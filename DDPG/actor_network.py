

import tensorflow as tf 
import numpy as np
import math


# Hyper Parameters
LAYER1_SIZE = 200
LAYER2_SIZE = 100
LEARNING_RATE = 0.001
TAO = 0.001
BATCH_SIZE = 64

class ActorNetwork:
    """docstring for ActorNetwork"""
    def __init__(self,state_size,action_size, action_upper_bound, action_lower_bound):
        self.action_upper_bound = action_upper_bound
        self.action_lower_bound = action_lower_bound
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.sess = tf.InteractiveSession()

            # create actor network
            self.state_input,\
            self.W1,\
            self.b1,\
            self.W2,\
            self.b2,\
            self.W3,\
            self.b3,\
            self.action_output, \
            self.W_conv1, \
            self.b_conv1, \
            self.W_conv2, \
            self.b_conv2, \
            self.W_fc1, \
            self.b_fc1 = self.create_network(state_size, action_size)

            # create target actor network
            self.target_state_input,\
            self.target_W1,\
            self.target_b1,\
            self.target_W2,\
            self.target_b2,\
            self.target_W3,\
            self.target_b3,\
            self.target_action_output, \
            self.target_W_conv1, \
            self.target_b_conv1, \
            self.target_W_conv2, \
            self.target_b_conv2, \
            self.target_W_fc1, \
            self.target_b_fc1 = self.create_network(state_size, action_size)


            # update rules
            intermed_W1 = tf.mul(self.W1, TAO)
            intermed_target_W1 = tf.mul(self.target_W1, (1 - TAO))
            new_target_W1 = tf.add(intermed_target_W1, intermed_W1)

            intermed_b1 = tf.mul(self.b1, TAO)
            intermed_target_b1 = tf.mul(self.target_b1, (1 - TAO))
            new_target_b1 = tf.add(intermed_target_b1, intermed_b1)

            intermed_W2 = tf.mul(self.W2, TAO)
            intermed_target_W2 = tf.mul(self.target_W2, (1 - TAO))
            new_target_W2 = tf.add(intermed_target_W2, intermed_W2)

            intermed_b2 = tf.mul(self.b2, TAO)
            intermed_target_b2 = tf.mul(self.target_b2, (1 - TAO))
            new_target_b2 = tf.add(intermed_target_b2, intermed_b2)

            intermed_W3 = tf.mul(self.W3, TAO)
            intermed_target_W3 = tf.mul(self.target_W3, (1 - TAO))
            new_target_W3 = tf.add(intermed_target_W3, intermed_W3)

            intermed_b3 = tf.mul(self.b3, TAO)
            intermed_target_b3 = tf.mul(self.target_b3, (1 - TAO))
            new_target_b3 = tf.add(intermed_target_b3, intermed_b3)

            self.W1_update = tf.assign(self.target_W1, new_target_W1)
            self.b1_update = tf.assign(self.target_b1, new_target_b1)
            self.W2_update = tf.assign(self.target_W2, new_target_W2)
            self.b2_update = tf.assign(self.target_b2, new_target_b2)
            self.W3_update = tf.assign(self.target_W3, new_target_W3)
            self.b3_update = tf.assign(self.target_b3, new_target_b3)

            # update rules for conv parameters
            intermed_W_conv1 = tf.mul(self.W_conv1, TAO)
            intermed_target_W_conv1 = tf.mul(self.target_W_conv1, (1 - TAO))
            new_target_W_conv1 = tf.add(intermed_target_W_conv1, intermed_W_conv1)

            intermed_b_conv1 = tf.mul(self.b_conv1, TAO)
            intermed_target_b_conv1 = tf.mul(self.target_b_conv1, (1 - TAO))
            new_target_b_conv1 = tf.add(intermed_target_b_conv1, intermed_b_conv1)

            intermed_W_conv2 = tf.mul(self.W_conv2, TAO)
            intermed_target_W_conv2 = tf.mul(self.target_W_conv2, (1 - TAO))
            new_target_W_conv2 = tf.add(intermed_target_W_conv2, intermed_W_conv2)

            intermed_b_conv2 = tf.mul(self.b_conv2, TAO)
            intermed_target_b_conv2 = tf.mul(self.target_b_conv2, (1 - TAO))
            new_target_b_conv2 = tf.add(intermed_target_b_conv2, intermed_b_conv2)

            intermed_W_fc1 = tf.mul(self.W_fc1, TAO)
            intermed_target_W_fc1 = tf.mul(self.target_W_fc1, (1 - TAO))
            new_target_W_fc1 = tf.add(intermed_target_W_fc1, intermed_W_fc1)

            intermed_b_fc1 = tf.mul(self.b_fc1, TAO)
            intermed_target_b_fc1 = tf.mul(self.target_b_fc1, (1 - TAO))
            new_target_b_fc1 = tf.add(intermed_target_b_fc1, intermed_b_fc1)

            self.W_conv1_update = tf.assign(self.target_W_conv1, new_target_W_conv1)
            self.b_conv1_update = tf.assign(self.target_b_conv1, new_target_b_conv1)
            self.W_conv2_update = tf.assign(self.target_W_conv2, new_target_W_conv2)
            self.b_conv2_update = tf.assign(self.target_b_conv2, new_target_b_conv2)
            self.W_fc1_update = tf.assign(self.target_W_fc1, new_target_W_fc1)
            self.b_fc1_update = tf.assign(self.target_b_fc1, new_target_b_fc1)

            # define training rules

            self.q_gradient_input = tf.placeholder("float",[None,action_size])
            self.parameters = [self.W1,self.b1,self.W2,self.b2,self.W3,self.b3,
                               self.W_conv1, self.b_conv1, self.W_conv2, self.b_conv2, self.W_fc1, self.b_fc1]
            #compute gradients of q
            #compute gradients of actor

            self.parameters_gradients = tf.gradients(self.action_output,self.parameters,self.q_gradient_input/BATCH_SIZE)
            self.optimizer = tf.train.AdamOptimizer(LEARNING_RATE).apply_gradients(zip(self.parameters_gradients,self.parameters))

            # Summaries
            self.summary_writer = tf.train.SummaryWriter('/tmp/logs/ddpg', self.sess.graph)
            self.filter_summary_b1 = tf.histogram_summary("q gradient input", self.q_gradient_input)
            self.filter_summary_b2 = tf.histogram_summary("b2 actor", self.b2)
            self.merged_summary_op = tf.merge_summary(
                [self.filter_summary_b1, self.filter_summary_b2])  # Merge all summaries into a single operator
            self.sess.run(tf.initialize_all_variables())

            self.saver = tf.train.Saver()
            checkpoint = tf.train.get_checkpoint_state("saved_networks")
            if checkpoint and checkpoint.model_checkpoint_path:
                self.saver.restore(self.session, checkpoint.model_checkpoint_path)
                print "Successfully loaded:", checkpoint.model_checkpoint_path
            else:
                print "Could not find old network weights"

            # copy target parameters
            self.sess.run([
                self.target_W1.assign(self.W1),
                self.target_b1.assign(self.b1),
                self.target_W2.assign(self.W2),
                self.target_b2.assign(self.b2),
                self.target_W3.assign(self.W3),
                self.target_b3.assign(self.b3)
            ])



    def create_network(self,state_size,action_size):
        def conv2d(x, W):
            return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

        def max_pool_2x2(x):
            return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                                  strides=[1, 2, 2, 1], padding='SAME')
        layer1_size = LAYER1_SIZE
        layer2_size = LAYER2_SIZE

        state_input = tf.placeholder("float",[None,state_size[0],state_size[1], state_size[2]], name='state_input_placeholder')

        self.W1_assign_placeholder = tf.placeholder(tf.float32, shape=[1024, layer1_size])
        self.b1_assign_placeholder = tf.placeholder(tf.float32, shape=[layer1_size])
        self.W2_assign_placeholder = tf.placeholder(tf.float32, shape=[layer1_size, layer2_size])
        self.b2_assign_placeholder = tf.placeholder(tf.float32, shape=[layer2_size])
        self.W3_assign_placeholder = tf.placeholder(tf.float32, shape=[layer2_size, action_size])
        self.b3_assign_placeholder = tf.placeholder(tf.float32, shape=[action_size])

        self.W_conv1_assign_placeholder = tf.placeholder(tf.float32, shape=[5, 5, 2, 32])
        self.b_conv1_assign_placeholder = tf.placeholder(tf.float32, shape=[32])
        self.W_conv2_assign_placeholder = tf.placeholder(tf.float32, shape=[5, 5, 32, 64])
        self.b_conv2_assign_placeholder = tf.placeholder(tf.float32, shape=[64])
        self.W_fc1_assign_placeholder = tf.placeholder(tf.float32, shape=[7 * 7 * 64, 200])
        self.b_fc1_assign_placeholder = tf.placeholder(tf.float32, shape=[200])

        W1 = self.variable([200, layer1_size], 200)
        b1 = self.variable([layer1_size], 200)
        W2 = self.variable([layer1_size, layer2_size], layer1_size)
        b2 = self.variable([layer2_size], layer1_size)
        W3 = self.variable([layer2_size, action_size], 0.0003)
        b3 = self.variable([action_size], 0.0003)

        W_conv1 = self.variable([5, 5, 2, 32], state_size[0]*state_size[1])
        b_conv1 = self.variable([32], state_size[0]*state_size[1])
        h_conv1 = tf.nn.relu(conv2d(state_input, W_conv1) + b_conv1)
        h_pool1 = max_pool_2x2(h_conv1)

        W_conv2 = self.variable([5, 5, 32, 64], 32)
        b_conv2 = self.variable([64], 32)
        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        h_pool2 = max_pool_2x2(h_conv2)

        W_fc1 = self.variable([7 * 7 * 64, 200], 7.0*7.0*64.0)
        b_fc1 = self.variable([200], 7.0*7.0*64.0)
        h_conv2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
        h_fc1 = tf.nn.relu(tf.matmul(h_conv2_flat, W_fc1) + b_fc1)

        layer1 = tf.nn.relu(tf.matmul(h_fc1,W1) + b1)
        layer2 = tf.nn.relu(tf.matmul(layer1,W2) + b2)
        action_output = tf.mul(tf.tanh(tf.matmul(layer2,W3) + b3),
                                      self.action_upper_bound-self.action_lower_bound)
        #                        -np.max([abs(self.action_upper_bound),abs(self.action_lower_bound)])) #add a sigmoid activation

        return state_input, W1, b1, W2, b2, W3, b3, action_output, W_conv1, b_conv1, W_conv2, b_conv2, W_fc1, b_fc1

    def train(self,q_gradient_batch,state_batch):
        self.sess.run(self.optimizer,feed_dict={
            self.q_gradient_input:q_gradient_batch,
            self.state_input:state_batch
            })

    def evaluate(self,state_batch):
        return self.sess.run(self.action_output,feed_dict={
            self.state_input:state_batch
            })

    def get_action(self,state):
        return self.sess.run(self.action_output,feed_dict={
            self.state_input:[state]
            })[0]


    def target_evaluate(self,state_batch):
        return self.sess.run(self.target_action_output,feed_dict={
            self.target_state_input:state_batch
            })

    def update_target(self):

        self.sess.run([
            self.W1_update,
            self.b1_update,
            self.W2_update,
            self.b2_update,
            self.W3_update,
            self.b3_update,
            self.W_conv1_update,
            self.b_conv1_update,
            self.W_conv2_update,
            self.b_conv2_update,
            self.W_fc1_update,
            self.b_fc1_update
        ])


    # f fan-in size
    def variable(self,shape,f):
        return tf.Variable(tf.random_uniform(shape,-1/math.sqrt(f),1/math.sqrt(f)))

    def update_summaries(self, q_gradient_batch, state_batch):
        summary_str = self.sess.run(self.merged_summary_op, feed_dict={
            self.q_gradient_input: q_gradient_batch,
            self.state_input: state_batch
        })
        self.summary_writer.add_summary(summary_str)
    def save_network(self,time_step):
        self.saver.save(self.sess, 'DDPG/saved_networks/' + 'actor-network', global_step = time_step)




