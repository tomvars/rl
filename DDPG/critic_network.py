
import tensorflow as tf
import numpy as np
import math

LAYER1_SIZE = 200
LAYER2_SIZE = 100
TAO = 0.001

class CriticNetwork:
    """docstring for CriticNetwork"""
    def __init__(self,state_size = 4,action_size = 2, tanh=True, LEARNING_RATE=0.0001):
        self.graph = tf.Graph()
        with self.graph.as_default():
            self.sess = tf.InteractiveSession()

            # create q network
            self.state_input,\
            self.action_input,\
            self.W1,\
            self.b1,\
            self.W2,\
            self.W2_action,\
            self.b2,\
            self.W3,\
            self.b3, \
            self.q_value_output,\
            self.W_conv1, \
            self.b_conv1, \
            self.W_conv2, \
            self.b_conv2, \
            self.W_fc1, \
            self.b_fc1 = self.create_q_network(state_size, action_size, tanh)
            # create target q network (the same structure with q network)
            self.target_state_input,\
            self.target_action_input,\
            self.target_W1,\
            self.target_b1,\
            self.target_W2,\
            self.target_W2_action,\
            self.target_b2,\
            self.target_W3,\
            self.target_b3,\
            self.target_q_value_output,\
            self.target_W_conv1,\
            self.target_b_conv1,\
            self.target_W_conv2,\
            self.target_b_conv2,\
            self.target_W_fc1,\
            self.target_b_fc1= self.create_q_network(state_size,action_size, tanh)




            # update rules
            intermed_W1 = tf.mul(self.W1, TAO)
            intermed_target_W1 = tf.mul(self.target_W1, (1 - TAO))
            new_target_W1 = tf.add(intermed_target_W1, intermed_W1)

            intermed_b1 = tf.mul(self.b1, TAO)
            intermed_target_b1 = tf.mul(self.target_b1, (1 - TAO))
            new_target_b1 = tf.add(intermed_target_b1, intermed_b1)

            intermed_W2 = tf.mul(self.W2, TAO)
            intermed_target_W2 = tf.mul(self.target_W2, (1 - TAO))
            new_target_W2 = tf.add(intermed_target_W2, intermed_W2)

            intermed_W2_action = tf.mul(self.W2_action, TAO)
            intermed_target_W2_action = tf.mul(self.target_W2_action, (1 - TAO))
            new_target_W2_action = tf.add(intermed_target_W2_action, intermed_W2_action)

            intermed_b2 = tf.mul(self.b2, TAO)
            intermed_target_b2 = tf.mul(self.target_b2, (1 - TAO))
            new_target_b2 = tf.add(intermed_target_b2, intermed_b2)

            intermed_W3 = tf.mul(self.W3, TAO)
            intermed_target_W3 = tf.mul(self.target_W3, (1 - TAO))
            new_target_W3 = tf.add(intermed_target_W3, intermed_W3)

            intermed_b3 = tf.mul(self.b3, TAO)
            intermed_target_b3 = tf.mul(self.target_b3, (1 - TAO))
            new_target_b3 = tf.add(intermed_target_b3, intermed_b3)

            self.W1_update = tf.assign(self.target_W1, new_target_W1)
            self.b1_update = tf.assign(self.target_b1, new_target_b1)
            self.W2_update = tf.assign(self.target_W2, new_target_W2)
            self.W2_action_update = tf.assign(self.target_W2_action, new_target_W2_action)
            self.b2_update = tf.assign(self.target_b2, new_target_b2)
            self.W3_update = tf.assign(self.target_W3, new_target_W3)
            self.b3_update = tf.assign(self.target_b3, new_target_b3)

            # update rules for conv parameters
            intermed_W_conv1 = tf.mul(self.W_conv1, TAO)
            intermed_target_W_conv1 = tf.mul(self.target_W_conv1, (1 - TAO))
            new_target_W_conv1 = tf.add(intermed_target_W_conv1, intermed_W_conv1)

            intermed_b_conv1 = tf.mul(self.b_conv1, TAO)
            intermed_target_b_conv1 = tf.mul(self.target_b_conv1, (1 - TAO))
            new_target_b_conv1 = tf.add(intermed_target_b_conv1, intermed_b_conv1)

            intermed_W_conv2 = tf.mul(self.W_conv2, TAO)
            intermed_target_W_conv2 = tf.mul(self.target_W_conv2, (1 - TAO))
            new_target_W_conv2 = tf.add(intermed_target_W_conv2, intermed_W_conv2)

            intermed_b_conv2 = tf.mul(self.b_conv2, TAO)
            intermed_target_b_conv2 = tf.mul(self.target_b_conv2, (1 - TAO))
            new_target_b_conv2 = tf.add(intermed_target_b_conv2, intermed_b_conv2)

            intermed_W_fc1 = tf.mul(self.W_fc1, TAO)
            intermed_target_W_fc1 = tf.mul(self.target_W_fc1, (1 - TAO))
            new_target_W_fc1 = tf.add(intermed_target_W_fc1, intermed_W_fc1)

            intermed_b_fc1 = tf.mul(self.b_fc1, TAO)
            intermed_target_b_fc1 = tf.mul(self.target_b_fc1, (1 - TAO))
            new_target_b_fc1 = tf.add(intermed_target_b_fc1, intermed_b_fc1)

            self.W_conv1_update = tf.assign(self.target_W_conv1, new_target_W_conv1)
            self.b_conv1_update = tf.assign(self.target_b_conv1, new_target_b_conv1)
            self.W_conv2_update = tf.assign(self.target_W_conv2, new_target_W_conv2)
            self.b_conv2_update = tf.assign(self.target_b_conv2, new_target_b_conv2)
            self.W_fc1_update = tf.assign(self.target_W_fc1, new_target_W_fc1)
            self.b_fc1_update = tf.assign(self.target_b_fc1, new_target_b_fc1)

            # Define training optimizer
            self.y_input = tf.placeholder("float",[None,1], name='y_input_placeholder')
            self.cost = tf.pow(self.q_value_output-self.y_input,2)/tf.to_float(tf.shape(self.y_input)[0])
            + 0.0001*tf.reduce_sum(tf.pow(self.W2,2))+0.0001*tf.reduce_sum(tf.pow(self.b2,2))
            # + 0.0001 * tf.reduce_sum(tf.pow(self.W1, 2)) + 0.0001 * tf.reduce_sum(tf.pow(self.b1, 2))
            + 0.0001 * tf.reduce_sum(tf.pow(self.W3, 2)) + 0.0001 * tf.reduce_sum(tf.pow(self.b3, 2)) # added some more regularisation

            # self.optimizer = tf.train.AdamOptimizer(LEARNING_RATE).minimize(self.cost)

            #Gradient clipping
            opt_func = tf.train.AdamOptimizer(LEARNING_RATE)
            tvars = tf.trainable_variables()
            grads, _ = tf.clip_by_global_norm(tf.gradients(self.cost, tvars), 1)
            # capped_gradients = [(tf.clip_by_value(grad, -1., 1.), var) if grad!=None else (tf.clip_by_value(tf.constant(0.0), -1., 1.), var) for grad, var in gradients]
            self.optimizer = tf.train.AdamOptimizer(LEARNING_RATE).apply_gradients(zip(grads,tvars))

            self.action_gradients = tf.gradients(self.q_value_output,self.action_input)
            #self.action_gradients = [self.action_gradients_v[0]/tf.to_float(tf.shape(self.action_gradients_v[0])[0])]

            # Summaries
            self.summary_writer = tf.train.SummaryWriter('/tmp/logs/ddpg', self.sess.graph)
            self.filter_summary_y_input = tf.histogram_summary("y_input", self.y_input)
            self.filter_summary_b2 = tf.histogram_summary("b2 critic", self.b2)
            self.filter_summary_q_value_output = tf.histogram_summary("q_value_input", self.q_value_output)
            self.filter_summary_cost = tf.histogram_summary("cost", self.cost)
            # self.filter_summary_state = tf.image_summary("state", self.state_input)
            self.merged_summary_op = tf.merge_summary(
                [self.filter_summary_y_input, self.filter_summary_b2,
                 self.filter_summary_q_value_output, self.filter_summary_cost])
                 # self.filter_summary_state])  # Merge all summaries into a single operator

            self.sess.run(tf.initialize_all_variables())

            # saved networks
            # self.saver = tf.train.Saver()
            # checkpoint = tf.train.get_checkpoint_state("DDPG/saved_networks")
            # if checkpoint and checkpoint.model_checkpoint_path:
            #     self.saver.restore(self.sess, checkpoint.model_checkpoint_path)
            #     print "Successfully loaded:", checkpoint.model_checkpoint_path
            # else:
            #     print "Could not find old network weights"

            # copy target parameters
            self.sess.run([
                self.target_W1.assign(self.W1),
                self.target_b1.assign(self.b1),
                self.target_W2.assign(self.W2),
                self.target_W2_action.assign(self.W2_action),
                self.target_b2.assign(self.b2),
                self.target_W3.assign(self.W3),
                self.target_b3.assign(self.b3)
            ])



    def create_q_network(self,state_size,action_size, tanh):
        def conv2d(x, W):
            return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

        def max_pool_2x2(x, name):
            return tf.nn.max_pool(x, ksize=[1, 2, 2, 1],
                                  strides=[1, 2, 2, 1], padding='SAME', name=name)

        # the layer size could be changed
        layer1_size = LAYER1_SIZE
        layer2_size = LAYER2_SIZE

        state_input = tf.placeholder("float",[None,state_size[0],state_size[1], state_size[2]], name='state_input_placeholder')
        action_input = tf.placeholder("float",[None,action_size], name='action_input_placeholder')
        # action_input = tf.placeholder("float")
        # state_input = tf.placeholder("float")
        self.W1_assign_placeholder = tf.placeholder(tf.float32, shape=[200, layer1_size],name='W1_assign')
        self.b1_assign_placeholder = tf.placeholder(tf.float32, shape=[layer1_size],name='b1_assign')
        self.W2_assign_placeholder = tf.placeholder(tf.float32, shape=[layer1_size, layer2_size],name='W2_assign')
        self.W2_action_assign_placeholder = tf.placeholder(tf.float32, shape=[action_size, layer2_size],name='W2_action_assign')
        self.b2_assign_placeholder = tf.placeholder(tf.float32, shape=[layer2_size],name='b2_assign')
        self.W3_assign_placeholder = tf.placeholder(tf.float32, shape=[layer2_size, 1],name='W3_assign')
        self.b3_assign_placeholder = tf.placeholder(tf.float32, shape=[1],name='b3_assign')

        self.W_conv1_assign_placeholder = tf.placeholder(tf.float32, shape=[5,5,2,32],name='W_conv1_assign')
        self.b_conv1_assign_placeholder = tf.placeholder(tf.float32, shape=[32],name='b_conv1_assign')
        self.W_conv2_assign_placeholder = tf.placeholder(tf.float32, shape=[5, 5, 32, 64],name='W_conv2_assign')
        self.b_conv2_assign_placeholder = tf.placeholder(tf.float32, shape=[64],name='b_conv2_assign')
        self.W_fc1_assign_placeholder = tf.placeholder(tf.float32, shape=[7 * 7 * 64, 200],name='W_fc1_assign')
        self.b_fc1_assign_placeholder = tf.placeholder(tf.float32, shape=[200],name='b_fc1_assign')

        W_conv1 = self.variable([5, 5, 2, 32], state_size[0],name='W_conv1')
        b_conv1 = self.bias_variable([32], 10,name='b_conv1')
        W_conv2 = self.variable([5, 5, 32, 64], state_size[0],name='W_conv2')
        b_conv2 = self.bias_variable([64], 10,name='b_conv2')

        W_fc1 = self.variable([7 * 7 * 64, 200], state_size[0],name='W_fc1')
        b_fc1 = self.bias_variable([200], 10,name='b_fc1')

        W1 = self.variable([200,layer1_size],200,name='W1')
        b1 = self.variable([layer1_size],10,name='b1')
        W2 = self.variable([layer1_size,layer2_size],layer1_size+action_size,name='W2')
        W2_action = self.variable([action_size,layer2_size],layer1_size+action_size,name='W2_action')
        b2 = self.variable([layer2_size], 10,name='b2')
        W3 = self.variable([layer2_size,1],0.0003,name='W3')
        b3 = self.bias_variable([1],0.0003,name='b3')

        h_conv1 = tf.nn.relu(conv2d(state_input, W_conv1) + b_conv1, name='conv_layer1')
        h_pool1 = max_pool_2x2(h_conv1, name='max_pool_1')
        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2, name='conv_layer2')
        h_pool2 = max_pool_2x2(h_conv2, name='max_pool_2')

        h_conv2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
        h_fc1 = tf.nn.relu(tf.matmul(h_conv2_flat, W_fc1) + b_fc1, name='fully_connected_layer_1')

        layer1 = tf.nn.relu(tf.matmul(h_fc1,W1) + b1, name='extra_layer1')
        layer2 = tf.nn.relu(tf.matmul(layer1,W2) + tf.matmul(action_input,W2_action) + b2, name='extra_layer2')

        # Final layer is different for different tanh value.
        if tanh:
            q_value_output = tf.tanh(tf.matmul(layer2,W3) + b3)
        else:
            q_value_output = tf.matmul(layer2, W3) + b3

        return state_input,action_input,W1,b1,W2,W2_action,b2,W3,b3,q_value_output,\
               W_conv1,b_conv1,W_conv2,b_conv2,W_fc1,b_fc1

    def train(self,y_batch,state_batch,action_batch):
        #action = np.transpose([action_batch])

        self.sess.run(self.optimizer,feed_dict={
            self.y_input:np.transpose([y_batch]),
            self.state_input:state_batch,
            self.action_input:action_batch
            })


    def gradients(self,state_batch,action_batch):
        return self.sess.run(self.action_gradients,feed_dict={
            self.state_input:state_batch,
            self.action_input:action_batch
            })[0]

    def target_evaluate(self,state_batch,action_batch):
        return self.sess.run(self.target_q_value_output,feed_dict={
            self.target_state_input:state_batch,
            self.target_action_input:action_batch
            })

    def evaluate(self,state_batch,action_batch):
        return self.sess.run(self.q_value_output,feed_dict={
            self.state_input:state_batch,
            self.action_input:action_batch})

    def output_cost(self, state_batch, action_batch, y_batch):
        return self.sess.run(self.cost, feed_dict={
            self.state_input:state_batch,
            self.action_input:action_batch,
            self.y_input: np.transpose([y_batch])})

    def update_target(self):

        self.sess.run([
            self.W1_update,
            self.b1_update,
            self.W2_update,
            self.b2_update,
            self.W3_update,
            self.b3_update,
            self.W_conv1_update,
            self.b_conv1_update,
            self.W_conv2_update,
            self.b_conv2_update,
            self.W_fc1_update,
            self.b_fc1_update
        ])


    # f fan-in size
    def variable(self,shape,f, name):
        return tf.Variable(tf.random_uniform(shape,-1/math.sqrt(f),1/math.sqrt(f)), name=name)

    def bias_variable(self, shape, f, name):
        return tf.Variable(tf.random_uniform(shape, -f, f), name=name)

    def update_summaries(self, state_batch, action_batch, y_batch):
        summary_str = self.sess.run(self.merged_summary_op, feed_dict={
            self.y_input: np.transpose([y_batch]),
            self.state_input: state_batch,
            self.action_input: action_batch
        })
        self.summary_writer.add_summary(summary_str)

    def save_network(self,time_step):
        self.saver.save(self.sess, 'DDPG/saved_networks/' + 'critic-network', global_step = time_step)