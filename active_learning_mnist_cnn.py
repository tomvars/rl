import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import random
import matplotlib.pyplot as plt
from QUIRE_query import query
from bandits import UCB1
from sklearn.manifold import TSNE

lb = LabelEncoder()
ohe = OneHotEncoder()

data = pd.read_csv('mnist_CNN.csv', nrows=2000)

X = data.drop('class', axis=1)
y = data['class']

X = X.as_matrix()
max_number_of_samples = 100
methods = ['random', 'uncertain', 'quire']




def test_active_learner(X, y, number_of_trials=3, max_number_of_samples=30, method='quire', kernel='rbf'):
    final_f1 = []
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.90, random_state=2016)
    y_train = y_train.as_matrix()
    lmbda = 1.0
    if kernel == 'rbf':
        K = rbf_kernel(X_train)
    L = np.linalg.inv(K + lmbda * np.eye(len(X_train)))
    Q = np.ones(3)
    actions = np.arange(3)
    N = np.ones(3)
    for trial in range(0, number_of_trials):
        # Uncertainty sampling
        f1_scores, number_of_samples = [0.0], []
        X_labelled = X_train[:5]
        y_labelled = y_train[:5]
        X_unlabelled = np.delete(X_train, [0, 1, 2, 3, 4], 0)
        y_unlabelled = np.delete(y_train, [0, 1, 2, 3, 4], 0)
        Lindex = [0,1,2,3,4]
        Uindex = list(range(5,len(X_train)))
        t = 0
        for i in range(0, max_number_of_samples):
            t+=1
            svc = svm.SVC(kernel='linear', C=1.0, probability=True).fit(X_labelled, y_labelled)
            # SELECT A METHOD USING MULTI-ARMED BANDIT SETUP.
            # Monte Carlo Q updates
            if method == 'ALBL':
                arm = methods[UCB1(Q, t, actions, N)]
                print(arm)
                if arm == 'random':
                    query_index = random.randint(0, X_unlabelled.shape[0] - 1)
                    y_pred = svc.predict(X_test)
                    f1 = f1_score(y_test, y_pred, average='weighted')
                    reward = f1-f1_scores[-1]
                    Q[0] = Q[0] + (1.0/N[0]) * (reward - Q[0])
                    N[0]+=1
                    f1_scores.append(f1)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0][0]
                    Lindex.append(train_query_index)
                    Uindex.remove(train_query_index)

                elif arm == 'uncertain':
                    query_index = ((np.abs(svc.predict_proba(X_unlabelled) - 0.5)).argmin()) % X_unlabelled.shape[0]
                    y_pred = svc.predict(X_test)
                    f1 = f1_score(y_test, y_pred, average='weighted')
                    reward = f1-f1_scores[-1]
                    Q[1] = Q[1] + (1.0/N[1]) * (reward - Q[1])
                    N[1]+=1
                    f1_scores.append(f1)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0][0]
                    Lindex.append(train_query_index)
                    Uindex.remove(train_query_index)

                elif arm =='quire':
                    query_index = query(y_labelled, K, L, lmbda, Lindex, Uindex)
                    Lindex.append(query_index)
                    Uindex.remove(query_index)
                    y_pred = svc.predict(X_test)
                    f1 = f1_score(y_test, y_pred, average='weighted')
                    reward = f1-f1_scores[-1]
                    Q[2] = Q[2] + (1.0/N[2]) * (reward - Q[2])
                    N[2]+=1
                    f1_scores.append(f1)
                    point = X_train[query_index]
                    query_index_temp = np.where(np.all(X_unlabelled == point, axis=1))
                    query_index = query_index_temp[0][0]
            elif method == 'random':
                query_index = random.randint(0, X_unlabelled.shape[0] - 1)
                y_pred = svc.predict(X_test)
                f1_scores.append(f1_score(y_test, y_pred, average='weighted'))
            elif method == 'uncertain':
                query_index = ((np.abs(svc.predict_proba(X_unlabelled) - 0.5)).argmin()) % X_unlabelled.shape[0]
                y_pred = svc.predict(X_test)
                f1_scores.append(f1_score(y_test, y_pred, average='weighted'))
            elif method =='quire':
                query_index = query(y_labelled, K, L, lmbda, Lindex, Uindex)
                Lindex.append(query_index)
                Uindex.remove(query_index)
                y_pred = svc.predict(X_test)
                f1_scores.append(f1_score(y_test, y_pred, average='weighted'))
                point = X_train[query_index]
                query_index_temp = np.where(np.all(X_unlabelled == point, axis=1))
                query_index = query_index_temp[0][0]
            X_labelled = np.vstack((X_labelled, X_unlabelled[query_index]))
            y_labelled = np.append(y_labelled, y_unlabelled[query_index])
            X_unlabelled = np.delete(X_unlabelled, query_index, 0)
            y_unlabelled = np.delete(y_unlabelled, query_index)
            #####################################

            number_of_samples.append(X_labelled.shape[0])
        final_f1.append(f1_scores)
    return final_f1

# print('Uncertain...')
# uncertain_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='uncertain')
# print('Random...')
# random_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='random')
# print('QUIRE...')
# quire_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='quire')
# print('ALBL...')
# ALBL_UCB1_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='ALBL')
# ##Plotting##
# #
# # print(uncertain_final_f1)
# plt.figure()
# plt.title('F1 score for different number of samples', fontsize=16)
# plt.xlabel('Number of samples', fontsize=16)
# plt.ylabel('F1 score', fontsize=16)
# plt.errorbar(range(5, max_number_of_samples + 6), np.mean(uncertain_final_f1, axis=0),
#              yerr=np.var(uncertain_final_f1, axis=0), label='UNCERTAIN')
# plt.errorbar(range(5, max_number_of_samples + 6), np.mean(random_final_f1, axis=0),
#              yerr=np.var(random_final_f1, axis=0), label='RANDOM')
# plt.errorbar(range(5, max_number_of_samples + 6), np.mean(quire_final_f1, axis=0),
#              yerr=np.var(quire_final_f1, axis=0), label='QUIRE')
# plt.errorbar(range(5, max_number_of_samples + 6), np.mean(ALBL_UCB1_f1, axis=0),
#              yerr=np.var(ALBL_UCB1_f1, axis=0), label='ALBL UCB1')
# plt.legend(loc='lower right')
# plt.show()