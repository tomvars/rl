#!/usr/bin/env python3
"""
The script runs experiments to compare the performance of ALBL and other active
learning algorithms.
"""

import copy
import os
from data_loader import get_dataset, make_svmlight_file
import numpy as np
import matplotlib
# matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from sklearn.cross_validation import train_test_split
# from sklearn.datasets import dump_svmlight_file
from sklearn.datasets import load_svmlight_file
import datetime
import json
# libact classes
from libact.base.dataset import Dataset, import_libsvm_sparse
from libact.models import SVM
from libact.query_strategies import  UncertaintySampling, RandomSampling,\
    ActiveLearningByLearning, HintSVM, QUIRE
from libact.labelers import IdealLabeler
import pickle
import itertools
import time


def run(trn_ds, tst_ds, lbr, model, qs, quota):
    E_in, E_out = [], []

    for _ in range(quota):
        ask_id = qs.make_query()
        X, _ = zip(*trn_ds.data)
        lb = lbr.label(X[ask_id])
        trn_ds.update(ask_id, lb)

        model.train(trn_ds)
        E_in = np.append(E_in, model.score(trn_ds))
        E_out = np.append(E_out, model.score(tst_ds))

    return E_in, E_out


def split_train_test(dataset_filepath, test_size, n_labeled):
    X, y = import_libsvm_sparse(dataset_filepath).format_sklearn()

    X_train, X_test, y_train, y_test = \
        train_test_split(X, y, test_size=test_size)

    while len(np.unique((y_train[:n_labeled]))) != 2:
        X_train, X_test, y_train, y_test = \
            train_test_split(X, y, test_size=test_size)

    trn_ds = Dataset(X_train, np.concatenate(
        [y_train[:n_labeled], [None] * (len(y_train) - n_labeled)]))
    tst_ds = Dataset(X_test, y_test)
    fully_labeled_trn_ds = Dataset(X_train, y_train)

    return trn_ds, tst_ds, y_train, fully_labeled_trn_ds


def main(dataset, path_for_run, n_experiments, n_labels, **kwargs):
    # Specifiy the parameters here:
    # path to your binary classification dataset
    config_filename = path_for_run + '/config.json'
    progress_filename = path_for_run + '/progress.json'
    config_dict = {'DATA':dataset, 'n_experiments': n_experiments,
                   'PATH': path_for_run, 'n_labels':n_labels,
                   'n_rows': kwargs['nrows'], 'arms': ['Random', 'Uncertain', 'Uncertain 2', 'Hint SVM', 'ALBL']}
    progress_dict = {'TIME TAKEN': 0, 'Strategies done':0, 'Strategies to do': 5*n_experiments}
    with open(config_filename, 'w') as fp:
        json.dump(config_dict, fp)
    with open(progress_filename, 'w') as fp:
        json.dump(progress_dict, fp)
    t0 = time.time()
    times = []

    ALC_data = ['hiva', 'nova', 'ibn_sina', 'sylva', 'zebra']
    if dataset in ALC_data:
        make_svmlight_file(dataset, **kwargs)
        ds_name = '%s_svmlight' % dataset
        path = os.getcwd()
        # dataset_filepath = os.path.join(
        #     os.path.dirname(os.path.realpath(__file__)), 'ALC_data/%s.txt' % ds_name)
        dataset_filepath = path + '/ALC_data/%s.txt' % ds_name
    else:
        raise NameError
    test_size = 0.8    # the percentage of samples in the dataset that will be
                        # randomly selected and assigned to the test set
    n_labeled = 2      # number of samples that are initially labeled
    result1, result2, result3, result4, result5 = [], [], [], [], []
    results = []
    for T in range(n_experiments): # repeat the experiment 20 times
        print("%dth experiment" % (T+1))

        trn_ds, tst_ds, y_train, fully_labeled_trn_ds = \
            split_train_test(dataset_filepath, test_size, n_labeled)
        # print(trn_ds, tst_ds, y_train, fully_labeled_trn_ds)
        trn_ds2 = copy.deepcopy(trn_ds)
        trn_ds3 = copy.deepcopy(trn_ds)
        trn_ds4 = copy.deepcopy(trn_ds)
        trn_ds5 = copy.deepcopy(trn_ds)
        lbr = IdealLabeler(fully_labeled_trn_ds)

        # quota = len(y_train) - n_labeled    # number of samples to query
        quota = n_labels

        # Comparing UncertaintySampling strategy with RandomSampling.
        # model is the base learner, e.g. LogisticRegression, SVM ... etc.
        print('Random...')
        qs1 = RandomSampling(trn_ds)
        model = SVM(kernel='linear', decision_function_shape='ovr', class_weight='balanced')
        _, E_out_1 = run(trn_ds2, tst_ds, lbr, model, qs1, quota)
        result1.append(E_out_1.tolist())
        t1 = time.time()
        times.append(t1 - t0)
        progress_dict = {'TIME TAKEN': times, 'Strategies done': T * 5 + 2, 'Strategies to do': 5 * n_experiments}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)

        print('Uncertain...')
        qs2 = UncertaintySampling(trn_ds2,
                                 model=SVM(decision_function_shape='ovr'))
        model = SVM(kernel='linear', decision_function_shape='ovr', class_weight='balanced')
        _, E_out_2 = run(trn_ds, tst_ds, lbr, model, qs2, quota)
        result2.append(E_out_2.tolist())
        t1 = time.time()
        times.append(t1-t0)
        progress_dict = {'TIME TAKEN': times, 'Strategies done': T*5 + 1, 'Strategies to do': 5 * n_experiments}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)

        print('Uncertain_margin...')
        qs3 = UncertaintySampling(trn_ds3,
                                 model=SVM(decision_function_shape='ovr'), method='sm')
        model = SVM(kernel='linear', decision_function_shape='ovr', class_weight='balanced')
        _, E_out_3 = run(trn_ds, tst_ds, lbr, model, qs3, quota)
        result3.append(E_out_3.tolist())
        t1 = time.time()
        times.append(t1 - t0)
        progress_dict = {'TIME TAKEN': times, 'Strategies done': T * 5 + 3, 'Strategies to do': 5 * n_experiments}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)

        print('HINTSVM...')
        qs4 = QUIRE(trn_ds4, cl=1.0, ch=1.0)
        model = SVM(kernel='linear', decision_function_shape='ovr', class_weight='balanced')
        _, E_out_4 = run(trn_ds4, tst_ds, lbr, model, qs4, quota)
        result4.append(E_out_4.tolist())
        t1 = time.time()
        times.append(t1 - t0)
        progress_dict = {'TIME TAKEN': times, 'Strategies done': T * 5 + 4, 'Strategies to do': 5 * n_experiments}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)

        print('ALBL...')
        qs5 = ActiveLearningByLearning(trn_ds5,
                    query_strategies=[
                        UncertaintySampling(trn_ds5,
                                            model=SVM(kernel='linear',
                                                      decision_function_shape='ovr',
                                                      ),
                                            method = 'lc'),
                        UncertaintySampling(trn_ds5,
                                            model=SVM(kernel='linear',
                                                      decision_function_shape='ovr',
                                                      ),
                                            method='sm'),
                        QUIRE(trn_ds5)
                    ],
                    T=quota,
                    uniform_sampler=True,
                    model=SVM(kernel='linear', decision_function_shape='ovr')
                )
        model = SVM(kernel='linear', decision_function_shape='ovr', class_weight='balanced')
        _, E_out_5 = run(trn_ds5, tst_ds, lbr, model, qs5, quota)
        result5.append(E_out_5.tolist())
        t1 = time.time()
        times.append(t1 - t0)
        progress_dict = {'TIME TAKEN': times, 'Strategies done': T * 5 + 5, 'Strategies to do': 5 * n_experiments}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)
    # results = [result1.tolist(), result2.tolist(), result3.tolist(), result4.tolist(), result5.tolist()]
    results = [result1, result2, result3, result4, result5]
    json.dump(results, open(path_for_run + '/%s_results.json' % dataset, 'w'))


if __name__ == '__main__':

    # PARAMS
    nrows = [1000]
    ALC_data = ['hiva', 'nova', 'ibn_sina', 'sylva']
    n_experiments = [1]
    n_labels = [100]
    grid = [ALC_data, n_experiments, n_labels, nrows]
    runs = list(itertools.product(*grid))

    # Document file config in run directory
    PATH = os.getcwd()
    now = datetime.datetime.now()
    datetime_string = now.strftime('%d-%m-%y-%H-%M-%S')
    path_for_run = PATH + '/ALBL_logs/' + datetime_string
    config_filename = path_for_run + '/config.json'

    progress_filename = path_for_run + '/progress.json'
    if not os.path.exists(os.path.dirname(config_filename)):
        os.makedirs(os.path.dirname(config_filename))

    for RUN in runs:
        main(RUN[0], path_for_run, RUN[1], RUN[2], nrows=RUN[3])
        # main('hiva')
