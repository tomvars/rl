import os
import pandas as pd
import pickle
import json
from zipfile import ZipFile
import urllib
from tempfile import mktemp
from sklearn.datasets import dump_svmlight_file, load_svmlight_file
import numpy as np

def fetch_data(filename, **kwargs):
    if not os.path.isfile('ALC_data/%s.data' % filename): # Does the file exist?
        temp_location = mktemp('.zip')
        urllib.request.urlretrieve('http://www.causality.inf.ethz.ch/al_data/%s.zip' % filename, temp_location)
        thefile = ZipFile(temp_location)
        thefile.extractall('ALC_data')
        thefile.close()


def fetch_labels():
    ALC_data = ['alex', 'hiva', 'ibn_sina', 'nova', 'orange', 'sylva', 'zebra']
    existing_labels = []
    for filename in ALC_data: # check for all labels in 'Labels'
        existing_labels.append(os.path.isfile('ALC_data/Labels/%s.label' % filename))
    if not np.all(np.array(existing_labels)):
        labels_url = 'http://www.causality.inf.ethz.ch/al_data/Labels.zip'
        temp_location = mktemp('.zip')
        urllib.request.urlretrieve(labels_url, temp_location)
        thefile = ZipFile(temp_location)
        thefile.extractall('ALC_data')
        thefile.close()


def get_dataset(filename, **kwargs):

    path = os.getcwd()
    ALC_data = ['hiva', 'ibn_sina', 'nova', 'orange', 'sylva', 'zebra']

    if filename in ALC_data:
        fetch_data(filename, **kwargs)
        fetch_labels()
        df = pd.read_csv(path + '/ALC_data/' + str(filename) + '.data', delim_whitespace=True, **kwargs)
        df2 = pd.read_csv(path + '/ALC_data/Labels/' + str(filename) + '.label', **kwargs)
        X = df.as_matrix()
        y = df2.as_matrix().flatten()
    elif filename == 'tractable':
        y = pickle.load(open('Tractable_tsne_python2_labels.p', 'rb'))
        X = pickle.load(open('Tractable_tsne_python2.p', 'rb'))
    elif filename == 'mnist_pixels':
        y = json.load(open('MNIST_raw_pixels_labels.json', 'r'))
        X = json.load(open('MNIST_pixels.json', 'r'))
    else:
        raise Exception("invalid name '"+filename+"' needs to be : 'hiva', 'ibn_sina', 'nova', 'orange', 'sylva', 'zebra'")
    return X, y

def make_svmlight_file(filename, **kwargs):
    PATH = os.getcwd()
    nrows = kwargs['nrows']
    file_exists = os.path.isfile('ALC_data/%s_svmlight.txt' % filename)  # Does the file exist?
    wrong_nrows = False
    if file_exists:
        X, y = load_svmlight_file(open('ALC_data/%s_svmlight.txt' % filename, 'rb'))
        if len(y) != nrows:
            wrong_nrows = True  # Is the file of the same size?
    if not(file_exists) or (file_exists and wrong_nrows):
        print('got here')
        X, y = get_dataset(filename, **kwargs)
        dump_svmlight_file(X,y,open('ALC_data/%s_svmlight.txt' % filename, 'wb'))

# make_svmlight_file('nova', nrows=999)
# print(X,y)