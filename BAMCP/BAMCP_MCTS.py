import numpy as np
import random
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.mplot3d import axes3d
import pydot
import os
import pickle
path_to_file = os.getcwd()

class BAMCP:
    def __init__(self):
        pass


class Frozenlake:

    def __init__(self, board_size=5):
        self.g = [random.choice([i for i in range(board_size)]),
                  random.choice([i for i in range(board_size)])]  # goal
        self.o = [random.choice([i for i in range(board_size) if i!=self.g[0]]),
                  random.choice([i for i in range(board_size) if i!=self.g[1]])] # obstacle
        self.g = [2,2]
        self.o = [2,3]
        self.board = np.ones((board_size, board_size)) * -0.1
        self.board[self.g[0]][self.g[1]] = 1.0
        self.board[self.o[0]][self.o[1]] = -1.0
        self.N = np.ones((board_size, board_size, 4))
        self.N_sh= np.ones((board_size, board_size, 4))
        self.N_sha = np.ones((board_size, board_size, 4))
        self.Q = np.zeros((board_size, board_size, 4))
        self.board_size = board_size

    def __str__(self):
        return str(self.board)


class Episode(Frozenlake):

    def __init__(self, board):
        self.actions = []
        self.starting_point = [random.choice([i for i in range(board.board_size) if i!=board.g[0] and i!=board.o[0]]),
                                    random.choice([i for i in range(board.board_size) if i != board.g[1] and i != board.o[1]])]
        self.points = [self.starting_point]
        self.current_point = self.starting_point
        self.terminated = False
        self.reward = 0
        self.board_size = board.board_size
        self.board = board.board

    def move(self, board, number_of_episodes, policy='e-greedy'):
        e = random.random()  # e-greedy policy
        if policy == 'e-greedy':
            if e > 1.0/20:
                action = np.argmax(board.Q[self.current_point[0]][self.current_point[1]])
            else:
                action = random.choice([0, 1, 2, 3])  # Random policy
        elif policy == 'UCT':
            c = 0.2
            action = np.argmax(board.Q[self.current_point[0]][self.current_point[1]] +
                               c * np.sqrt(
                                   np.log(
                                       board.N_sh[self.current_point[0]][self.current_point[1]]/
                                       board.N_sha[self.current_point[0]][self.current_point[1]][self.last_action])))

        cardinals = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
        if action == cardinals['N']:
            self.current_point = [(self.current_point[0]-1)%self.board_size, self.current_point[1]]
            self.actions.append(0)
            self.last_action = 0
        elif action == cardinals['E']:
            self.current_point = [self.current_point[0], (self.current_point[1]+1)%self.board_size]
            self.actions.append(1)
            self.last_action = 1
        elif action == cardinals['S']:
            self.current_point = [(self.current_point[0]+1)%self.board_size, self.current_point[1]]
            self.actions.append(2)
            self.last_action = 2
        elif action == cardinals['W']:
            self.current_point = [self.current_point[0], (self.current_point[1]-1)%self.board_size]
            self.actions.append(3)
            self.last_action = 3
        self.reward += self.board[self.current_point[0]][self.current_point[1]]
        self.points.append(self.current_point)
        if self.board[self.current_point[0]][self.current_point[1]] == -1 or\
                self.board[self.current_point[0]][self.current_point[1]] == 1:
            self.terminated = True

    def episode_length(self):
        return len(self.points)

    def __str__(self):
        return str(self.actions)


def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / N


def plot_action_graph(ep):
    action_num = 0
    for action in ep.actions:
        actions = list(range(0, 4))
        # actions.remove(action)
        action_num += 1
        if action_num == len(ep.actions):
            edge = pydot.Edge(str(action_num) + ', ' + str(action), str(action_num + 1) + ', ' + "%s" % ep.last_action)
            graph.add_edge(edge)
        else:
            for i in actions:
                edge = pydot.Edge(str(action_num) + ', ' + str(action), str(action_num + 1) + ', ' + "%s" % i)
                graph.add_edge(edge)
    graph.write_png(path_to_file + '/action_graph.png')

def train_with_MC(board_size):
    number_of_episodes = 1000000
    board = Frozenlake(board_size=board_size)
    rewards = []
    for e in range(number_of_episodes):
        episode = Episode(board)
        while not episode.terminated:
            episode.move(board, e)
            board.N[episode.points[-1][0]][episode.points[-1][1]][episode.last_action] += 1
        for i in range(episode.episode_length() - 1):
            board.Q[episode.points[i][0]][episode.points[i][1]][episode.actions[i]] += \
                (episode.reward - board.Q[episode.points[i][0]][episode.points[i][1]][episode.actions[i]]) / \
                board.N[episode.points[i][0]][episode.points[i][1]][episode.actions[i]]

        rewards.append(episode.reward)
    return board, rewards

def plot_Q_values(Q_values):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    X, Y = np.mgrid[0:5, 0:5]
    Z = np.maximum.reduce(Q_values, axis=2)
    ax.plot_wireframe(X, Y, Z)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Value')
    plt.show()

if __name__ == "__main__":
    graph = pydot.Dot(graph_type='graph')
    Q_values = pickle.load(open('FrozenLakeQ.p','rb'))
    board = Frozenlake
    board.Q = Q_values
    episode = Episode(board)
    episode.tree_policy()
    episode.rollout_policy()