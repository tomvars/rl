import numpy as np
import random
import matplotlib.pyplot as plt
import pydot
import os
import pickle
path_to_file = os.getcwd()

class BAMCP:
    def __init__(self):
        pass


class Frozenlake:

    def __init__(self, board_size=5):
        self.g = [random.choice([i for i in range(board_size)]),
                  random.choice([i for i in range(board_size)])]  # goal
        self.o = [random.choice([i for i in range(board_size) if i!=self.g[0]]),
                  random.choice([i for i in range(board_size) if i!=self.g[1]])] # obstacle
        self.g = [2,2]
        self.o = [2,3]
        self.board = np.ones((board_size, board_size)) * -0.1
        self.board[self.g[0]][self.g[1]] = 1.0
        self.board[self.o[0]][self.o[1]] = -1.0
        self.N = np.ones((board_size, board_size, 4))
        self.Q = np.zeros((board_size, board_size, 4))
        self.board_size = board_size

    def __str__(self):
        return str(self.board)


class Episode(Frozenlake):

    def __init__(self, board):
        self.actions = []
        self.starting_point = [random.choice([i for i in range(board.board_size) if i!=board.g[0] and i!=board.o[0]]),
                                    random.choice([i for i in range(board.board_size) if i != board.g[1] and i != board.o[1]])]
        self.points = [self.starting_point]
        self.current_point = self.starting_point
        self.terminated = False
        self.reward = 0
        self.board_size = board.board_size
        self.board = board.board

    def move(self, board, number_of_episodes, greedy=False):
        e = random.random()  # e-greedy policy
        if not greedy:
            if e > 1.0/float(number_of_episodes+1):
                action = np.argmax(board.Q[self.current_point[0]][self.current_point[1]])
            else:
                action = random.choice([0, 1, 2, 3])  # Random policy
        else:
            action = np.argmax(board.Q[self.current_point[0]][self.current_point[1]])  # choose action greedily
        cardinals = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
        if action == cardinals['N']:
            self.current_point = [(self.current_point[0]-1)%self.board_size, self.current_point[1]]
            self.actions.append(0)
            self.last_action = 0
        elif action == cardinals['E']:
            self.current_point = [self.current_point[0], (self.current_point[1]+1)%self.board_size]
            self.actions.append(1)
            self.last_action = 1
        elif action == cardinals['S']:
            self.current_point = [(self.current_point[0]+1)%self.board_size, self.current_point[1]]
            self.actions.append(2)
            self.last_action = 2
        elif action == cardinals['W']:
            self.current_point = [self.current_point[0], (self.current_point[1]-1)%self.board_size]
            self.actions.append(3)
            self.last_action = 3
        self.reward += self.board[self.current_point[0]][self.current_point[1]]
        self.points.append(self.current_point)
        if self.board[self.current_point[0]][self.current_point[1]] == -1 or\
                self.board[self.current_point[0]][self.current_point[1]] == 1:
            self.terminated = True

    def episode_length(self):
        return len(self.points)

    def __str__(self):
        return str(self.actions)


def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0))
    return (cumsum[N:] - cumsum[:-N]) / N


def plot_action_graph(ep):
    action_num = 0
    for action in ep.actions:
        actions = list(range(0, 4))
        # actions.remove(action)
        action_num += 1
        if action_num == len(ep.actions):
            edge = pydot.Edge(str(action_num) + ', ' + str(action), str(action_num + 1) + ', ' + "%s" % episode.last_action)
            graph.add_edge(edge)
        else:
            for i in actions:
                edge = pydot.Edge(str(action_num) + ', ' + str(action), str(action_num + 1) + ', ' + "%s" % i)
                graph.add_edge(edge)
    graph.write_png(path_to_file + '/action_graph.png')

if __name__ == "__main__":
    graph = pydot.Dot(graph_type='graph')
    number_of_episodes = 100000
    board = Frozenlake(board_size=5)
    print(board)
    rewards = []
    for e in range(number_of_episodes):
        episode = Episode(board)
        while not episode.terminated:
            episode.move(board, e)
            board.N[episode.points[-1][0]][episode.points[-1][1]][episode.last_action] += 1
        for i in range(episode.episode_length()-1):
            board.Q[episode.points[i][0]][episode.points[i][1]][episode.actions[i]] += \
                (episode.reward - board.Q[episode.points[i][0]][episode.points[i][1]][episode.actions[i]])/\
                board.N[episode.points[i][0]][episode.points[i][1]][episode.actions[i]]
        if e % 1000 == 0:
            rewards.append(episode.reward)
            # print(episode.reward)
    pickle.dump(board.Q, open('FrozenLakeQ.p','wb'))
    # Output board.
    preferred_actions = [['' for i in range(board.board_size) ] for j in range(board.board_size)]
    cardinal_actions = ['N','E','S','W']
    print(board.g, board.o)
    for i in range(board.board_size):
        for j in range(board.board_size):
            action = np.argmax(board.Q[i][j])
            if i == board.g[0] and j == board.g[1]:
                preferred_actions[i][j] = 'g'
            elif i == board.o[0] and j == board.o[1]:
                preferred_actions[i][j] = 'o'
            else:
                preferred_actions[i][j] = cardinal_actions[action]

    for row in preferred_actions:
        print(row)

    # Q = board.Q.reshape(25,4)
    # plt.matshow(Q, cmap=plt.cm.gray)
    print(board)
    plt.figure()
    plt.title('Reward against episodes using a running mean of window size 10', fontsize = 20)
    plt.xlabel('Iterations in 1000s', fontsize = 18)
    plt.ylabel('Averaged reward', fontsize= 18)
    plt.plot(running_mean(rewards,10), '-')
    # plot_action_graph(episode)
    plt.show()



