import matplotlib.pyplot as plt
import numpy as np
import random
mean = [1,0]
cov = [[0.05,0],[0,0.05]]
x1, y1 = np.random.multivariate_normal(mean, cov, 50).T

mean = [1,0.8]
cov = [[0.02,0],[0,0.02]]
x2, y2 = np.random.multivariate_normal(mean, cov, 20).T

mean = [0.2,0]
cov = [[0.02,0],[0,0.02]]
x3, y3 = np.random.multivariate_normal(mean, cov, 20).T

mean = [0,1]
cov = [[0.05,0],[0,0.05]]
x4, y4 = np.random.multivariate_normal(mean, cov, 50).T

mean = [0.8,1]
cov = [[0.02,0],[0,0.02]]
x5, y5 = np.random.multivariate_normal(mean, cov, 20).T

mean = [0,0.2]
cov = [[0.02,0],[0,0.02]]
x6, y6 = np.random.multivariate_normal(mean, cov, 20).T


plt.figure()
plt.scatter(x1,y1, color='red')
plt.scatter(x2,y2, color='red')
plt.scatter(x3,y3, color='red')
plt.scatter(x4,y4, color='blue')
plt.scatter(x5,y5, color='blue')
plt.scatter(x6,y6, color='blue')
plt.show()