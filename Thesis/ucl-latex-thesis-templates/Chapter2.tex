\chapter{Active Learning as a multi-armed bandit task}\label{al}

In the last chapter three strategies for a reinforcement learning active learning setup were introduced. In this chapter two of these strategies will be discussed, one where the set of actions is to label or not to label a point (stream-based sampling) and another which is a multi-armed bandit where the arms of the bandit correspond to active learning heuristics.

\section{Active Learning by Learning (ALBL)}
The title of this section corresponds to the name of the paper by Hsu and Lin. In it they describe a method of ensembling active learning heuristics by using a multi-armed bandit. In this section the method in ALBL is described and evaluated on a set of standard data sets. Past work of a similar nature will also be discussed and extensions/modifications are presented.

\subsection{Multi-armed bandit}\label{MAB}

The multi-armed bandit problem (MAB) is a scenario where a learner is given access to a finite number of $k$ arms which it may pull to receive a reward. At each iteration the learner will choose an action $A$ which is to pull on of the arms and will receive a reward $R$ from the environment. After $t$ time steps the learner will have a history $h_t = [ <A_1,R_1>, <A_2,R_2>, ... , <A_t, R_t>]$ which is the information it must use to inform its next action. The key to this problem is the balance between \textit{Exploration} and \textit{Exploitation}. The learner could keep pulling the arm which gave it a positive reward, or it could explore to see if other arms will produce higher reward. The objective function in this scenario which we want to minimize is the regret, $L$, which is defined as $L_t = \sum^t_{i=1} v_{*} - q(a_i)$ where $v_{*}$ is the optimal value that could have been achieved if at each timestep $n$ the learner had acted optimally and $q(a)$ is the expected reward of taking action $a$. One approach to minimising regret is the idea of \textit{"optimism in the face of uncertainty"}. We can model our beliefs about the value of an action $q(a)$ as a probability distribution $P[q(a)]$ where the variance of the distribution is a function of the number of times the action has been chosen $N(a)$. It is then possible to estimate an upper confidence bound for each action value $q(a)$. What follows is a derivation of the UCB1 algorithm which utilises upper confidence bounds to achieve logarithmic expected regret in the MAB. It comes from Auer et al. 2002 with notation borrowed from [HADO LECTURES REF]\\

\textbf{Theorem 1 (Hoeffding's Inequality)} : \textit{Let $\Sigma_{1}, ..., \Sigma_{t}$ be i.i.d random variables $\in [0,1]$ and let $\bar{X}_{t} = \frac{1}{t} \sum_{i=1}^{t} \Sigma_{i}$ be the sample mean. Then $P[E[X] > \hat{X}_t +u] \leq e^{-2tu^{2}}$} \\

 By utilizing Theorem 1 and applying it to the bandit problem with a bounded reward, e.g $R \in [0,1]$. We have that,
\begin{equation}
P[q(a) > Q_t(a) + U_t(a)] < e^{-2N_t(a)U_t(a)^2}
\end{equation}
Where the left hand side is the probability that the true value of an action $a$ exceeds the upper confidence bound. If we consider this to be probability $p$ then we at the boundary we have that,
\[
p = e^{-2N_t(a)U_t(a)^2}
\]

Solving for $U_t(a)$,

\begin{equation}\label{eq:U}
U_t(a) = \sqrt{\frac{- \log p}{2N_t(a)}}
\end{equation}
Then a choice is made for $p$ as a function of the number of timesteps $t$ (should get smaller as the number of timesteps increases) of $p=t^{-4}$. Subbing into equation $\ref{eq:U}$ the final UCB1 algorithm is,
\[
a_t = \argmax_{a \in A} Q_t(a) + \sqrt{\frac{2 \log t}{N_t(a)}}
\]

This algorithm was implemented and is one of the multi-armed bandit algorithms discussed in Chapter $\ref{al}$

\subsection{Past work}

This idea of ensembling active learning algorithms was first applied by Yoram et al \cite{baram2004online} in 2004. The topic was reexplored by \cite{ganti2013building}, where in their literature review they claim that to their knowledge no other work had linked multi-armed bandits and active learning before. Hsu and Lin cite \cite{baram2004online} but do not mention the work of \cite{ganti2013building} possibly due to the significant differences in their approaches. Given the fact that all of these approaches are similar and long, a thorough description of ALBL is introduced and differences to other approaches are highlighted.\\

ALBL works by proposing active learning as a multi-armed bandit problem as introduced in section $\ref{MAB}$. The arms of the bandit are a choice of active learning algorithms which will choose a point to label according to a heuristic such as the ones mentioned in the last chapter. The two challenges when conceiving of active learning as a MAB are the choice of bandit algorithm and the design of the reward function. As labels are applied and $\mathit{L}$ grows the performance of the classifier has an upward bias regardless of the point chosen to be labeled. Additionally, the reward received for using a particular active learning algorithm will vary over time as $\mathit{L}$ and $\mathit{U}$ change. This is the justification for using what is known as the \textit{adversarial setting} of the bandit problem \cite{auer2002nonstochastic}. In this setting no assumptions are made about the distribution of rewards as were made in the UCB1 derivation. Instead it is assumed that an adversary is changing the reward and "playing against you". In ALBL the EXP4.P algorithm from Beygelzimer et al 2011\cite{beygelzimer2011contextual} is used as the bandit algorithm with some modifications.\\

\subsection{Choice of algorithm}

The UCB1 algorithm was introduced in section $\ref{MAB}$ and considers the reward distribution for each arm to be stochastic and independent. For the ALBL method EXP4.P was used, an algorithm which works by maintaining a set of weights $\textbf{w}$ on a group of $K$ experts which are converted to a probability vector $\textbf{p}(t) \in [p_{min},1]$ where $p_{min} \in [0,1/K]$. An arm is chosen according to these probabilities, which in ALBL correspond to different active learning algorithms, specifically \textsc{UNCERTAIN}, \textsc{RANDOM}, \textsc{QUIRE} and \textsc{PSDS} which were introduced in section $\ref{ALH}$.\\

After choosing which arm to pull, a point is selected according to the selected active learning algorithm. Each $j$th unlabelled point $x_j \in \mathit{U}$ is assigned a probability of being chosen by each $k$th algorithm and stored in a probability vector $\psi_{j}^{k} (t)$ which is updated at each timestep $t$. This vector is combined with the probability of choosing an arm $\textbf{p}(t)$ to sample a point $x^* \in \mathit{U}$ at the $t$th iteration based on $q_j = \sum^{K}_{k=1} p_k(t) \psi^k_j (t)$ which is the probability of sampling a point $x_j \in \mathit{U}$ at timestep $t$. The detailed EXP4.P algorithm is shown in Figure $\ref{exp4p}$ taken from the original paper. It shows how the probability vectors $p(t)$ and the weights $w$ are updated at each time step as a function of the chosen action and the received reward. In line 1 of the algorithm there are some advice vectors, $\bm{\xi}^{1}(t), ... , \bm{\xi}^{N}(t)$ which are used to update $p_j$ in step 2 as well as $w_i(t+1)$ in step 6. These advice vectors are designed to inject context into EXP4.P. In the original paper the following example is used to illustrate their functionality, \\

Consider articles served to users on a website, the objective is to maximise the total number of user clicks on these recommended articles. The arms of the multi-armed bandit are the available articles in this formulation. Context is introduced by having a set of beliefs about users and the articles they will enjoy. These are quantised by clustering an array of input user feature vectors $\textbf{X} = [ \textbf{f}_1, \textbf{f}_2, ..., \textbf{f}_N ]$ into a finite number of $B$ clusters. Then using the normalized Euclidean distances of each user feature vector to each cluster centre, a membership feature $\textbf{d}$ of size $B$ is created. Additionally, there needs to exist a mapping of user clusters to articles $a \in {1, ..., K}^B$ where each $a_b$ is the article to be displayed to user from cluster $b$. Finally, the advice vector from an expert $\textbf{a}$ is $\xi^{\textbf{a}}_j = \sum_{b:a_b = j} d_b$. These advice vectors have a higher value for the articles which match up with the cluster membership of the user.\\

Although compelling for use in recommendation systems this method of context injection is not easily applicable to the MAB setting in active learning. In order to inject context in this way there would need to be a mapping between some input vector clustering and a particular active learning algorithm which is not available. In the ALBL paper, the authors don't comment on the fact that EXP4.P, which is designed as a contextual bandit algorithm, has been used without context. Practically, the authors' implementation of the ALBL algorithm set the advice vectors $\bm{\xi}$ to the identity matrix \cite{libact}.

\begin{figure}
\centering
\includegraphics[scale=2.5]{EXP4_algorithm.png}
\caption{EXP4.P algorithm from Beygelzimer et al \cite{beygelzimer2011contextual}}\label{exp4p}
\end{figure}

\subsection{Choice of reward function}

As mentioned earlier, one challenge of the MAB setting for active learning ensembling is the choice of MAB algorithm. The other is the choice of reward function which is discussed in this section. Ideally the best reward signal for an active learning by learning approach would be based on the test accuracy. This would mean that for a new labelled point added to a training set we would know what effect this had on our classifiers performance directly. In practice, having a sufficiently large labelled test set violates the setting for the expensive label problem we are attempting to tackle. Whilst useful for evaluating active learning algorithms, the test accuracy should not be used as a reward signal for this reason. Alternatively, the training accuracy could be used. However, for small samples (even with leave-one-out cross validation) this training accuracy can be biased and not resemble the test accuracy as presented in Baram et al. The solution proposed in the COMB paper from Baram et al utilises a human-designed metric in order to estimate the test accuracy. This metric called \textit{Cross-entropy Maximisation} (CEM) works by using the Bernoulli binary entropy $H(p)$ which is defined as $H(p) = H(1-p) = -p \log p - (1-p) \log (1-p)$. The $p$ in this case is chosen as $|\frac{C^{+1}(\mathit{U}}{|\mathit{U}|}|$ where $C^{+1}(U)$ denotes the number of points in the unlabelled set $\mathit(U)$ which have been positively classified by the current classifier. This makes CEM a monotonic function with a peak at $p=0.5$. In their paper the authors provide empirical evidence for the success of CEM as a substitute for the test accuracy in certain conditions and empirical evidence for its failure under others. Specifically, if we consider an unlabelled clustered landscape where there is one cluster of positive class with most points and a few others with a small number of points of the negative class then CEM will be successful. In this scenario the growth rate of CEM will be similar to that of the test accuracy as the discovery of more small clusters will lead to a class balance and therefore an increase in CEM and test accuracy. In the counterexample, the positive class is distributed amongs many clusters which contain most of the data and the negative class is a single cluster of few points. In this case as the active learning heuristics explore the dense positive clusters the test accuracy will increase as the positive class becomes better represented. However, the CEM will not increase in this case as the classes are not becoming more balanced. This failure of the CEM criterion is motivation for a better reward function which is agnostic to the data landscape of the task at hand.\\

The ALBL paper provides a more robust and unbiased reward function than the one used in COMB. Their solution is called \textit{Importance-Weighted accuracy} (IW-ACC) and works by using knowledge about the probability distribution of the sampling process in order to remove the sampling bias. As mentioned in the description of the EXP4.P algorithm a probability vector $q_j(t)$ denotes the probability of selecting a point $x_j \in \mathit{U}$ at a timestep $t$. Let $(x_i, y_i)$ denote a point and its label in the unlabelled set. Let $s_i \in {0,1}$ denote whether whether or not point $x_i$ was sampled or not and let $c_i = || y_i = f(x_i)||$, i.e whether or not the label $y_i$ was predicted correctly by the classifier $f$ applied to the point $x_i$. The average classification error over the whole unlabelled set of size $N_U$ is $\frac{1}{N_U} \sum_{i=1}^{N_U} ||y_i = f(x_i)||$. This would be the ideal success metric, however the set of $y_i$ labels is unknown. Instead, using the values defined an unbiased estimator of this true test accuracy can be evaluated as $\frac{1}{N_U} \sum_{i=1}^{N_U} s_i \frac{c_i}{q_i}$. Only the sampled points where $s_i = 1$ will count towards this accuracy which is not subject to sampling bias because of the normalization factor of $q_i$. The final IW-ACC for a classifier $f$ at iteration $\tau$ is shown in Equation $\ref{eq:iwacc}$. In this equation $i_t$ denotes the point sampled at time $t$.

\begin{equation}\label{eq:iwacc}
\mathit{IW-ACC}(f,\tau) = \frac{1}{N_U \tau} \sum^{\tau}_{i_t=1} \frac{c_{i_t}}{q_{i_t}}
\end{equation}


- EXP4 and each point as bandit for COMB

\subsection{Evaluation of ALBL}

-Discussion of methods used and contextual bandit algorithm that has no context, criticise paper\\
-Results with exp4 (verifying paper)\\
-Application and performance with higher dimensional data

\begin{figure}[H]
\centering
\includegraphics[scale=0.33]{hiva_accuracy.png}
\includegraphics[scale=0.33]{ibn_sina_accuracy.png}
\includegraphics[scale=0.33]{nova_accuracy.png}
\includegraphics[scale=0.33]{sylva_accuracy.png}
\caption{}
\end{figure}

\subsection{Experiments with other non-contextual bandit algorithms}

\begin{table}[]
\centering

\label{my-label}
\begin{tabular}{llllll}
 & f1 & acc & pre & rec & auc \\
Random & 0/\textbf{4}/0 & 0/\textbf{4}/0 & 0/\textbf{4}/0 & 0/\textbf{4}/0 & 0/\textbf{4}/0 \\
Uncertain 2 & \textbf{4}/0/0 & \textbf{4}/0/0 & \textbf{4}/0/0 & \textbf{3}/1/0 & \textbf{4}/0/0 \\
Uncertain & \textbf{4}/0/0 & \textbf{4}/0/0 & \textbf{2}/\textbf{2}/0 & \textbf{3}/1/0 & \textbf{4}/0/0 \\
uncertain entropy & \textbf{4}/0/0 & \textbf{4}/0/0 & \textbf{3}/1/0 & \textbf{4}/0/0 & \textbf{3}/1/0 \\
quire & \textbf{4}/0/0 & \textbf{4}/0/0 & \textbf{3}/1/0 & \textbf{2}/\textbf{2}/0 & \textbf{3}/1/0 \\
ALBL-UCB & \textbf{4}/0/0 & \textbf{4}/0/0 & 1/\textbf{3}/0 & \textbf{2}/\textbf{2}/0 & \textbf{4}/0/0

\end{tabular}
\caption{Table showing Win/Tie/Loss for different active learning algorithms as compared to RND for a given success metric}
\end{table}




-Compare and contrast UCB1, E-greedy and EXP4.P

Why you can't use this in practice.

\section{Contextual bandit algorithms applied to active learning}
\subsection{Bayes-adaptive Monte Carlo Planning applied to Active Learning}\label{bamcp}
-Motivation for its use\\
-How it works\\
-Why it doesn't work for Active Learning, and therefore why its hard to inject context\\
-How it could work?\\\cite{guez2014bayes}
\section{Future work in the multi-armed bandit domain}
Inject context as topology features.\\
Inject context as svm features. \\


\label{chapterlabel2}

