\chapter{Introduction}

\section{Motivation: The expensive label problem}
Before the advent of multi-layer neural networks there was no lack of labelled data for machine learning. When pioneering experiments into single-layer perceptrons took place in the 1960s which could distinguish male from female faces the limiting factor was not how many images could be provided. Recent progress has taken deep learning from research groups to industry and consumer applications. This shift has changed the nature of the data from general freely available data like images of men and women to more user and case specific data which can sometimes  be proprietary. If these complex neural networks are to be trained successfully on said data then tens of thousands of labeled examples must be provided [reference]. Obtaining such labels is a laborious process which requires, by definition, a human to label them at a particular cost. These labels can come at different costs, from a few cents for some image labelling tasks to hundreds of thousands of dollars to obtain the three dimensional structure of a protein. This is the motivation for the field of \textit{Active Learning}, which in the words of Burr Settles $"$is the study of machine learning systems that improve by asking questions$"$. The learner chooses the data that it learns from by looking at the unlabelled data and choosing a point to label. This process is referred to as \textit{querying}.
\section{Aims and Research Questions}

\section{Active Learning: An Introduction}\label{ali}

The motivation for active learning has been introduced. The nature of the data and the problem will call for a particular active learning scenario. Additionally, different querying strategies will have different levels of success for a given setup. In this section these scenarios will be introduced first and then a broad but not exhaustive list of active learning heuristics will be discussed. For a more thorough introduction to the field of Active Learning the reader is referred to the book "Active Learning" by Burr Settles. \cite{settles2010active}.
\subsection{Active learning scenarios}
In the case where there is a generative model of the data, $P(x|\mu)$ (where $\mu$ is some vector of parametors for a probability distribution), a sample ($x_1$) may be drawn from this probability distribution. This generated instance can then be presented to a human or oracle which we assume knows the true label. This scenario is known as \textit{query synthesis} and has had some success in real-world applications. However, an issue which arises from it is the generation of non-sensical samples. For example, hand-written digits being generated which don't correspond to a real number as seen in Figure $\ref{fig:digits}$. The top-right image is ambiguous and doesn't seem to correspond to any real digit.

\begin{figure}[H]
\centering
\includegraphics[scale=1.0]{nonsense_digits.png}
\caption{Result of query synthesis from neural network interpretation of handwritten digits. \cite{baum1992query}}\label{fig:digits}
\end{figure}
This problem of non-sensical samples from a generative model is discussed in Chapter $\ref{al}$. \\

If queries are not synthetic then they must come from a real input source. The other two active learning scenarios draw samples from real input sources but do so in different ways. In stream-based sampling a learner is presented with an input sample in an online way and may choose to either label or discard the sample. The learner will use its knowledge base to decide whether or not a point is worth labelling. This may be done by assigning a utility to each sample and then taking a weighted random choice to either label or not label. Alternatively a region of uncertainty can be defined in the input space and only samples which lie within it would be sampled. In section $\ref{bamcp}$ a method for applying reinforcement learning to stream-based active learning is discussed.\\

The most common scenario for active learning is referred to as \textit{Pool-Based Sampling}. In this paradigm, we have access to a large unlabelled data set $D_U$ and a labelled data set $D_L$, given these we must choose a member $x \in D_U$ to label at each query episode. This choice is normally made by assigning a utility to each point and choosing greedily, however in this thesis a new method is proposed to choose a point to label directly from $D_U$ and $D_L$ through a parametrised function. This method is discussed and evaluated in Chapter $\ref{drl}$.
\subsection{Active learning heuristics}\label{ALH}
The reason why these querying strategies are referred to as heuristics in this thesis is because they are approximate techniques which rely on certain assumptions and don't adapt to the task at hand. Nevertheless, these techniques can outperform random sampling for tasks with real noisy data. The evaluation framework for these techniques and some results from other papers are discussed in section $\ref{eval}$. What follows is an explanation of three very fundamental active learning techniques as well as a specific technique called QUIRE \cite{huang2010active} which combines some of these ideas.

\subsection*{Uncertain sampling}

In active learning the learner chooses a point to label to improve its understanding of the data. Following directly from this idea it makes sense for the learner to choose the points for which it is most uncertain about as these will contain the most unknown information. This strategy is known as \textit{Uncertain Sampling} and can be quantified in a few different ways.\\

If we consider a binary classification task where a label $y$ is infered from some input data $x$ a probabilistic classifier with parameters $\theta$ will output $P_{\theta}(y|x)$. In this binary task if $P_{\theta}(y|x)$ is exactly 0.5 then the classifier has the belief that both labels have equal probability of corresponding to $x$. Therefore, $x$ is a maximally uncertain point and should be labelled next according to this uncertain sampling. This metric for uncertainty is known as \textit{Least Confident} and could be applied to multi-class problems by querying $x* = \argmin_x P_{\theta}(\hat{y}|x)$. A better technique for multi-class problems is to label the point with the smallest difference between the two most likely labels, i.e $x* = \argmin_x [P_{\theta}(\hat{y_1}|x) - P_{\theta}(\hat{y_2}|x)]$ \cite{settles2010active} where $\hat{y_1}$ and $\hat{y_2}$ are the first and second most likely labels respectively. This metric is known as the \textit{Margin} and is an improvement but is still ignorant of the other label probabilities. In order to incorporate information about the other labels one can use the Shannon entropy, choosing a query according to $x* = \argmax_x - \sum_{y} P_{\theta}(y|x)\log P_{\theta} (y|x)$. This metric captures a variable's "average information content" \cite{settles2010active}.\\

In the experiments presented in Chapter $\ref{al}$ different Uncertain Sampling approaches are compared.

%\subsubsection*{Query-by-committee}


\subsection*{Density-weighted methods}

So far methods concerned with the information content of data have been discussed. These techniques don't incorporate any notion of how representative these data points are of the underlying distribution. There are four scenarios in Figure $\ref{fig:i_r}$ which highlight the difference between informative and representative points. In the leftmost example we see a binary classification problem with blue and red points denoting two different classes and the dotted line the classifier which separates them. The circles around the points are a measure of the distribution of the unlabelled points in some space. In the next illustration some labels have been removed but the unlabelled distributions have stayed the same; the classifier is positioned according to the labelled instances without utilising the unlabelled distributions. In the third illustration the unlabelled distributions have been used to position the classifier without using the labelled instances. Finally, in the last illustration the classifier has used both the labelled examples and the unlabelled distributions.

\begin{figure}[H]
\centering
\includegraphics[scale=1.0]{informative_representative.png}
\caption{Left: A binary classification problem. Centre Left: Approach favouring informative examples. Centre Right: Approach favoring representative examples. Right: Approach which uses informative and representative examples \cite{huang2010active} }
\end{figure}\label{fig:i_r}

In order to incorporate this density information to the querying operation a simple weighting can be applied based on the similarity of a point $x$ to all other points in $\mathcal{U}$. This weighting can be sumarised as follows,
\begin{equation}
x* = \argmax_x \phi_A (x) \times (\frac{1}{U} \sum_{x^{'} \in \mathcal{U}} sim(x,x'))^{\beta}
\end{equation}\label{eq:density}

Here the $\phi_A (x)$ represents some utility metric for point $x$ (e.g through uncertainty sampling) and $sim(x,x')$ is some similarity measure between point $x$ and every other point. The $\beta$ is a parameter used to weight the density component in the method.\\

This density-weighted approach forms the underlying principle behind QUIRE (querying informative and representative examples) and PSDS (Paired-Sampling in Density-Sensitive Active Learning) which form part of an ensembling technique discussed in Chapter $\ref{al}$ and are described in the next subsection.

\subsection*{QUIRE and PSDS}

QUIRE and PSDS are both density-weighted approaches to active learning. The reader is referred to \cite{huang2010active} and \cite{donmez2008paired} for more detailed explanations of the two techniques but this section will sketch how they work and how they differ.

The Paired-Sampling part of PSDS comes from the fact that at every iteration the objective is to maximise the chance of sampling from both sides a decision boundary. A technique is used to compute a density-sensitive distance measure which is based on the work of Chapelle et al. (2005) \cite{chapelle2005semi}. This is combined with a utility function $U(i,j)$ of a pair of points as the sum of the density estimate for each point. A score $S(i,j)$ is calculated for each pair of unlabelled points as $||\tilde{x_i}-\tilde{x_j}||^2 U(i,j)$ where $\tilde{x}$ represents a point which has been transformed onto a new space according to the aforementioned density-sensitive distance measure. This score function takes the same form as Equation $\ref{eq:density}$.

The method described by Sheng-Jun et al. (2010) called QUIRE has certain similarities to PSDS. They both require computing a distance kernel matrix $K(x_i,x_j)$ which stores the pairwise distances between all points for a particular kernel. The representativeness in QUIRE relies on estimating the possible labels for the points that are unlabelled such to minimise a loss function $L(D_l,D_u,\textbf{y}_u,\textbf{x}_s)$ where $D_l$ and $D_u$ are the labelled and unlabelled datasets, $\textbf{y}_u$ is the labels for the unlabelled instances and $\textbf{x}_s$ is the point to label. There is no explicit utility function for each data point, which means it does not follow the form in Equation $\ref{eq:density}$. Through the use of some approximations in manipulating the kernel matrix the algorithm complexity is reduced to $O(n_u)$; scales with respect to the unlabelled images.

QUIRE and PSDS form part of the Active Learning by Learning ensembling technique which is discussed in Chapter $\ref{al}$

\section{Evaluation of active learning techniques}\label{eval}

So far some active learning techniques have been introduced, but not a framework for evaluating them. In almost all of the active learning literature there is a familiar plot which is some success metric on the y-axis and the percentage or amount of labelled data on the x-axis (see Figure $\ref{fig:active_learning_example}$). A point $x$ is selected according to a query strategy, this point is labelled and added to a training set $D_l$ and a fixed classifier is trained on this updated data set, a success metric is then evaluated. In this way different query strategies can be compared according to these success metrics, however there is no overall concensus on which metric to use.

\begin{figure}[H]
\centering
\includegraphics[scale=0.6]{active_learning_TRACTABLE_df1_60_samples_1_trial.png}
\caption{Plot of F1 score against number of labelled examples for a fixed classifier and different active learning heuristics.}
\end{figure}\label{fig:active_learning_example}

In the work of Ramirez et al (2016) \cite{ramirez2016active} a meta-study was carried out which looked at what classifiers and what success metrics were used across 54 active learning papers. The results are shown in Figure $\ref{fig:metatable}$ and highlight the fact that the large majority of papers used single classifiers (83\%) and a single measure (91\%). However, the results also highlight a fragmentation in the choice of success metric and choice of classifier.

\begin{figure}[H]
\centering
\includegraphics[scale=2.3]{metatable.png}
\caption{ Table showing the classifier and success measure used across 54 papers. Taken from Ramirez et al (2016) \cite{ramirez2016active} }
\end{figure}\label{fig:metatable}

It is clear and also highlighted in the meta-study \cite{ramirez2016active} that model selection (choosing a classifier) can have a larger effect on the accuracy obtained than the active learning method. It is therefore necessary to choose a single model and to perform all active learning experiments on it. In this thesis an SVM was chosen as the fixed classifier for its ability to scale well with higher input dimensions as well as its success with linearly separable data. An implementation which uses stochastic gradient descent with an L2 Norm was used which allowed the classifier to have a "warm start". This enabled fast retraining of the classfier when a new point was added to the training set.\\

The baseline that needs to be beaten in order to justify an active learning heuristics is naturally random sampling. Although its a method which does not employ any of the information about the labelled and unlabelled data pool random sampling has the advantage of being unbiased. Sampling bias has been called "the most fundamental challenge posed by active learning" Dasgupta (2009)\cite{dasgupta2009two}, active learning heuristics avoid this by querying representative samples. In Ramirez et al uncertain sampling and query-by-Comittee are compared against random sampling across multiple datasets and multiple success metrics. The results showed that for the same classifier, the same data sets and the same active learning heuristic there was disagreement between area under the curve, accuracy, F1 score, precision and recall \cite{ramirez2016active}. This result was the motivation for reporting results in terms of different metrics in the coming chapters.

\section{Reinforcement Learning introduction}
According to Sutton and Barto "Reinforcement learning is learning what to do--how to map situations to actions--so as to maximize a numerical reward signal." It lives somewhere between supervised learning where we try to map directly from an input vector to a label and unsupervised learning where we look at the unlabelled data and draw inferences. We normally discuss the framework in terms of an agent and he environment. At each time step $t$ the agent executes an action $a_t$ and receives an observation or state representation $s_t$ and a scalar reward $r_t$. The agent uses the history of state, action, reward to inform its decision making process. 

-Talk about MDPs and POMDPs
-Talk about multi-armed bandits

\section{The challenge of state representation in the Active Learning domain}
An MDP as introduced in the previous section is the tuple $<A, S, P, R, \gamma>$. In order to consider active learning as a reinforcement learning problem there needs to be an appropriate representation for each of these values. One approach is to consider a broad end-to-end approach. The state is a representation of what we know and don't know about the data. This could be for example the raw pool of labelled, $\mathcal{L}$ and raw pool of unlabelled, $\mathcal{U}$ data points. The actions that need to be made are deciding which of the unlabelled points to label. This may be thought of in the pool labelling setting discussed in section $\ref{ali}$ in which case the set of actions $\mathcal{A}$ corresponds to all points $x \in \mathcal{U}$. This technique is explored in chapter BLAH. Alternatively the set of actions could be a stream-based model (See $\ref{ali}$) where each point is presented to the learner one by one and the possible actions are to label or not to label, this technique is explored in section BLAH. Chapter $\ref{al}$ introduces a technique that does't use either one of these set actions, instead the set of actions are one of a finite number of active learning heuristics.

\section{Choice of data}

The choice of data in active learning experiments is very broad despite efforts to create a unified testing framework. Papers studied in this thesis including Hsu et al, Baram et al and Huang et al compare their algorithms to one another but don't evaluate their performance on the same datasets. This is despite the formation of the Active Learning Challenge \cite{guyon2011results} in 2011 which presented six data sets for use in active learning experiments in an effort to unify the field and validate results. For the experiments carried out in Chapters $\ref{al}$ and $\ref{drl}$ the data was chosen according to different requirements. In order to evaluate the performance of Active Learning by Learning (ALBL) it was used on the aforementioned standard datasets. In Chapter $\ref{drl}$ experiments were run using MNIST for how standard it is in machine learning literature and the success of t-SNE when applied to the raw pixels. As this project was carried out at  Tractable, some proprietary data was also used in active learning experiments. The benefits of using this data was having a more robust and real dataset which clustered with more noise. Experiments were performed on 1036 dimensional convolutional neural network outputs which came from the penultimate layer of the networks. It has been shown that these penultimate layer representations can prove to be excellent lower-dimensional representations of the input images in order to apply SVMs and perform classification on them \cite{chatfield2014return}. Inference was performed on raw MNIST pixel data as well as convolutional layer output.

-All the data used in the active learning challenge \cite{guyon2011results}
-Comparison of MNIST in TSNE and Tractable data in TSNE.
-Conversation about dimensionality and connection to the state representation problem.
\label{chapterlabel1}
