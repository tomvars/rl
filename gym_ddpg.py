import gym
import time
import pickle
from ddpg import *
import time
import matplotlib.pyplot as plt
import datetime
import os
import json
import copy
import itertools

def output_Q_values():
    NUMBER_OF_SAMPLES=100
    experiment = 'ActiveLearning-v0'
    env = gym.make(experiment)
    agent = DDPG(env)
    env.monitor.start('/tmp/' + experiment + '-' + agent.name + '-experiment', force=True)
    observation = env.reset()
    agent.set_init_observation(observation)

    for i in range(10):
        action = env.action_space.sample()
        observation, reward, done, outputs = env.step(action)


    # Receive initial observation state s_1

    # sample different point randomly to estimate Q(s,a|theta)
    action_batch = [env.action_space.sample() for i in range(NUMBER_OF_SAMPLES)]
    # same observation, varying actions
    state_batch = [observation for i in range(NUMBER_OF_SAMPLES)]
    q_values = agent.critic_network.evaluate(state_batch,action_batch)
    print(q_values)

def main(EPISODES, STEPS, REPLAY_START_SIZE, REPLAY_BUFFER_SIZE= 1000, GAMMA = 0.95,
         tanh=True, LEARNING_RATE=0.0001, REWARD='RandomAccuracy'):

    def output_one_key_metric(accuracies, random_accuracy, ind):
        accuracy_list = accuracies[ind]
        sum_of_differences = np.sum(np.array(accuracy_list) - np.array(random_accuracy[:-1]))
        return sum_of_differences

    # Document file config in run directory
    PATH = os.getcwd()
    now = datetime.datetime.now()
    datetime_string = now.strftime('%d-%m-%y-%H-%M-%S')
    config_dict = {'EPISODES':EPISODES, 'STEPS':STEPS, 'REPLAY_START_SIZE':REPLAY_START_SIZE,
                   'tanh': tanh, 'LEARNING_RATE':LEARNING_RATE, 'REWARD': REWARD}
    config_filename = PATH + '/pickle_logs/' + datetime_string + '/config.json'
    progress_dict = {'EPISODES DONE':0, 'STEPS DONE':0, 'PROGRESS':0, 'TIME TAKEN':0, 'AUC':0}
    progress_filename = PATH + '/pickle_logs/' + datetime_string + '/progress.json'
    if not os.path.exists(os.path.dirname(config_filename)):
        os.makedirs(os.path.dirname(config_filename))
    with open(config_filename, 'w') as fp:
        json.dump(config_dict, fp)
    with open(progress_filename, 'w') as fp:
        json.dump(progress_dict, fp)
    random_accuracy = pickle.load(open('acc_using_random_mnist_pixels.p', 'rb'))

    experiment = 'ActiveLearning-v0' # Run the environment with the correct reward signal
    env = gym.make(experiment)
    env.reset()
    agent = DDPG(env, tanh=tanh, REPLAY_BUFFER_SIZE = REPLAY_BUFFER_SIZE,
                 REPLAY_START_SIZE = REPLAY_START_SIZE, LEARNING_RATE=LEARNING_RATE, GAMMA=GAMMA)
    results = []

    time_of_episodes = []
    env.monitor.start('/tmp/' + experiment + '-' + agent.name + '-experiment', force=True)
    rewards = np.zeros([EPISODES,STEPS])
    accuracies = np.zeros([EPISODES,STEPS])
    costs = []
    # plt.ion()
    # plt.axis([0, EPISODES*STEPS, 0,1])
    # plt.scatter(1, 1)
    # plt.pause(0.05)
    t0 = time.time()
    for i in xrange(EPISODES):
        observation = env.reset()
        # Receive initial observation state s_1
        agent.set_init_observation(observation)

        result = 0
        for t in xrange(STEPS):
            env.render()
            # Select action a_t
            if (i+1)*STEPS>REPLAY_START_SIZE:
                # action = agent.get_action()
                action = agent.get_q_argmax_action()
                # plt.scatter(i * t, agent.current_cost)
                # plt.pause(0.05)
                # plt.draw()
            else:
                action = env.action_space.sample()
            # Execute action a_t and observe reward r_t and observe new observation s_{t+1}
            observation, reward, done, outputs = env.step(action)
            # print(agent.critic_network.sess.run(agent.critic_network.cost.eval(), feed_dict=state_input:observation,)
            rewards[i][t] = reward
            accuracies[i][t] = outputs['acc']
            result += reward
            print(action)
            # Store transition(s_t,a_t,r_t,s_{t+1}) and train the network
            print(reward)
            print('COST:',agent.current_cost)
            costs.append(agent.current_cost)
            agent.set_feedback(observation, action, reward, done)
            # agent.actor_network.summary_writer.add_summary(agent.actor_network.filter_summary_W_conv1, t*i)
            print('EPISODE: ', i, ' Steps: ', t, ' result: ', result)
            #SAVE NETWORK WHEN Q VALUES ARE APPROPRIATE
            # if agent.q_sample < 1.0 and agent.q_sample > -1.0:
            #     agent.critic_network.save_network((i * STEPS) + t)
            #     print('SAVED CRITIC NETWORK VALUES!')
            if done:
                print('EPISODE: ', i, ' Steps: ', t, ' result: ', result)
                # result = 0
                break

        # Logging files
        t1 = time.time()
        results.append(output_one_key_metric(accuracies, random_accuracy, i))
        progress_dict = {'EPISODES DONE': i, 'STEPS DONE': (i+1)*STEPS,
                         'PROGRESS': "{percent:.2%}".format(percent=float((i+1))/float(EPISODES)),
                         'TIME TAKEN': t1-t0, 'AUC': results}
        with open(progress_filename, 'w') as fp:
            json.dump(progress_dict, fp)
        pickle.dump(rewards, open(PATH+'/pickle_logs/' + datetime_string + '/rewards.p', 'wb'))
        pickle.dump(accuracies, open(PATH+'/pickle_logs/' + datetime_string + '/accuracies.p', 'wb'))
        pickle.dump(costs, open(PATH + '/pickle_logs/' + datetime_string + '/costs.p', 'wb'))
    # agent.actor_network.save_network(EPISODES * STEPS)
    # agent.critic_network.save_network(EPISODES * STEPS)
    env.monitor.close()
    # plt.figure()
    # plt.title('Reward against Episode')
    # plt.ylabel('Reward')
    # plt.xlabel('Episode')
    # plt.plot(results)
    # plt.savefig('reward_%s_EPISODES_%s_STEPS_%s_REPLAY.png'%(EPISODES,STEPS,REPLAY_START_SIZE))

if __name__ == '__main__':
    # Parametrising the critic network:
    # Tanh or not tanh
    # Warm start vs not warm start
    # Learning rate
    # Gamma
    # Replay_start_size and replay_buffer_size
    # For each of these runs output the average accuracy per step over the last X episodes
    # --> get the AUC aswell

    # PARAM GRID
    EPISODES = [300]
    STEPS = [200]
    REPLAY_START_SIZE = [10000]
    REPLAY_BUFFER_SIZE = [1000]
    TANH = [True, False]
    LEARNING_RATE = [0.0001, 0.001]
    REWARD = ['FinalAccuracy','LastAccuracy','LastAccuracy']
    grid = [EPISODES, STEPS, REPLAY_START_SIZE, REPLAY_BUFFER_SIZE, TANH, LEARNING_RATE, REWARD]
    runs = list(itertools.product(*grid))

    list_of_dict_of_runs = []
    for i in runs:
        run = {'EPISODES':i[0], 'STEPS':i[1], 'REPLAY_START_SIZE':i[2], 'REPLAY_BUFFER_SIZE':i[3],
           'TANH': i[4], 'LEARNING_RATE':i[5], 'REWARD':i[6]}
        list_of_dict_of_runs.append(run)

    for i in list_of_dict_of_runs:
        ENV_CONFIG = {'STEPS': i['STEPS'], 'BINS': 28, 'REWARD': i['REWARD']}
        json.dump(ENV_CONFIG, open('ENV_CONFIG.json', 'w'))
        main(i['EPISODES'], i['STEPS'], i['REPLAY_START_SIZE'], REPLAY_BUFFER_SIZE=i['REPLAY_BUFFER_SIZE'],
         tanh=i['TANH'], LEARNING_RATE=i['LEARNING_RATE'], REWARD=i['REWARD'])

    # output_Q_values()
