import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import scipy.sparse
import random
import matplotlib.pyplot as plt
from QUIRE_query import query

lb = LabelEncoder()
ohe = OneHotEncoder()

data = pd.read_csv('mushroom_data.csv')
data = data.apply(lb.fit_transform)

# print(data)
X = data.drop('class', axis=1)
X = ohe.fit_transform(X)
y = data['class']
max_number_of_samples = 20


def test_active_learner(X, y, number_of_trials=2, max_number_of_samples=30, method='random', kernel='rbf'):
    final_f1 = []
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.90, random_state=2016)
    y_train = y_train.as_matrix()
    X_train = X_train.toarray()
    X_test = X_test.toarray()
    if method == 'quire':
        lmbda = 1.0
        if kernel == 'rbf':
            K = rbf_kernel(X_train)
        L = np.linalg.inv(K + lmbda * np.eye(len(X_train)))

    for trial in range(0, number_of_trials):
        # Uncertainty sampling
        f1_scores, number_of_samples = [], []
        X_labelled = X_train[:5]
        y_labelled = y_train[:5]
        X_unlabelled = np.delete(X_train, [1, 2, 3, 4, 5], 0)
        y_unlabelled = np.delete(y_train, [1, 2, 3, 4, 5])
        for i in range(0, max_number_of_samples):

            svc = svm.SVC(kernel='linear', C=1.0, probability=True).fit(X_labelled, y_labelled)

            if method == 'random':
                query_index = random.randint(0, X_unlabelled.shape[0] - 1)
            elif method == 'uncertain':
                query_index = ((np.abs(svc.predict_proba(X_unlabelled) - 0.5)).argmin()) % X_unlabelled.shape[0]
            elif method =='quire':
                query_index = query(y_labelled, K, L, lmbda, i+5)
            X_labelled = np.vstack((X_labelled, X_unlabelled[query_index]))
            y_labelled = np.append(y_labelled, y_unlabelled[query_index])
            X_unlabelled = np.delete(X_unlabelled, query_index, 0)
            y_unlabelled = np.delete(y_unlabelled, query_index)
            #####################################

            y_pred = svc.predict(X_test)

            f1_scores.append(f1_score(y_test, y_pred))
            number_of_samples.append(X_labelled.shape[0])
        final_f1.append(f1_scores)
    return final_f1


uncertain_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='uncertain')
random_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='random')
quire_final_f1 = test_active_learner(X, y, max_number_of_samples=max_number_of_samples, method='quire')

##Plotting##

plt.figure()
plt.title('F1 score for different number of samples', fontsize=16)
plt.xlabel('Number of samples', fontsize=16)
plt.ylabel('F1 score', fontsize=16)
plt.errorbar(range(5, max_number_of_samples + 5), np.mean(uncertain_final_f1, axis=0),
             yerr=np.var(uncertain_final_f1, axis=0), label='UNCERTAIN')
plt.errorbar(range(5, max_number_of_samples + 5), np.mean(random_final_f1, axis=0),
             yerr=np.var(random_final_f1, axis=0), label='RANDOM')
plt.errorbar(range(5, max_number_of_samples + 5), np.mean(quire_final_f1, axis=0),
             yerr=np.var(quire_final_f1, axis=0), label='QUIRE')
plt.legend(loc='lower right')
plt.show()
