import numpy as np

def query(y_labelled, K, L, lmbda, Lindex, Uindex):
    # Uindex = list(range(label_index, len(K)))
    # Lindex = list(range(0, label_index))
    query_index = -1
    min_eva = np.inf
    det_Laa = np.linalg.det(L[np.ix_(Uindex, Uindex)])
    # efficient computation of inv(Laa)
    M3 = np.dot(K[np.ix_(Uindex, Lindex)],
                np.linalg.inv(lmbda * np.eye(len(Lindex))))
    M2 = np.dot(M3, K[np.ix_(Lindex, Uindex)])
    M1 = lmbda * np.eye(len(Uindex)) + K[np.ix_(Uindex, Uindex)]
    inv_Laa = M1 - M2
    iList = list(range(len(Uindex)))
    if len(iList) == 1:
        return Uindex[0]
    for i, each_index in enumerate(Uindex):
        # go through all unlabeled instances and compute their evaluation
        # values one by one
        Uindex_r = Uindex[:]
        Uindex_r.remove(each_index)
        iList_r = iList[:]
        iList_r.remove(i)
        inv_Luu = inv_Laa[np.ix_(iList_r, iList_r)] - 1 / inv_Laa[i, i] * \
                                                      np.dot(inv_Laa[iList_r, i], inv_Laa[iList_r, i].T)
        tmp = np.dot(
            L[each_index][Lindex] -
            np.dot(
                np.dot(
                    L[each_index][Uindex_r],
                    inv_Luu
                ),
                L[np.ix_(Uindex_r, Lindex)]
            ),
            y_labelled,
        )
        eva = L[each_index][each_index] - \
              det_Laa / L[each_index][each_index] + 2 * np.abs(tmp)

        if eva < min_eva:
            query_index = each_index
            min_eva = eva
    return query_index
