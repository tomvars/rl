import pickle
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os
EPISODES = 73
STEPS = 200
REPLAY = 1000
HOUR = 19
MIN = 48

path = os.getcwd()
rewards = pickle.load(open(path + '/pickle_logs/rewards_'+str(EPISODES)+'_EPISODES_'+str(STEPS)+'_STEPS_'+
                           str(REPLAY)+'_REPLAY_'+str(HOUR)+'_hour_'+str(MIN)+'_minute.p', 'rb'))
accuracies = pickle.load(open(path + '/pickle_logs/accuracies_'+str(EPISODES)+'_EPISODES_'+str(STEPS)+'_STEPS_'+
                              str(REPLAY)+'_REPLAY_'+str(HOUR)+'_hour_'+str(MIN)+'_minute.p', 'rb'))
costs = pickle.load(open(path + '/pickle_logs/costs_'+str(EPISODES)+'_EPISODES_'+str(STEPS)+'_STEPS_'+
                         str(REPLAY)+'_REPLAY_'+str(HOUR)+'_hour_'+str(MIN)+'_minute.p', 'rb'))

# rewards = pickle.load(open(path + '/pickle_logs/rewards_' + str(EPISODES) + '_EPISODES_' + str(STEPS) + '_STEPS_' +
#                            str(REPLAY) + '_REPLAY.p', 'rb'))
# accuracies = pickle.load(
#     open(path + '/pickle_logs/accuracies_' + str(EPISODES) + '_EPISODES_' + str(STEPS) + '_STEPS_' +
#          str(REPLAY) + '_REPLAY.p', 'rb'))
# costs = pickle.load(open(path + '/pickle_logs/costs_' + str(EPISODES) + '_EPISODES_' + str(STEPS) + '_STEPS_' +
#                          str(REPLAY) + '_REPLAY.p', 'rb'))
random_accuracy = pickle.load(open('acc_using_random_mnist_pixels.p', 'rb'))

plt.figure()
plt.title('Reward per step averaged over episodes')
plt.xlabel('steps')
plt.ylabel('Reward')
plt.plot(np.mean(rewards, axis=0), 'b-', label='random difference')

plt.figure()
plt.title('Reward per Episode averaged over episodes')
plt.xlabel('Episodes')
plt.ylabel('Reward')
plt.plot(np.mean(rewards, axis=1), 'b-', label='random difference')
plt.legend(loc='best')

plt.figure()
plt.title('Final accuracy per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Accuracies', fontsize=18)
plt.plot(np.array(accuracies).T[99], 'b-')

plt.figure()
plt.title('Average accuracy per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Accuracies', fontsize=18)
plt.errorbar(range(np.mean(accuracies, axis=1).size), np.mean(accuracies, axis=1), yerr= np.std(accuracies, axis=1))
# plt.plot(np.mean(accuracies_alternative, axis=1), 'r-')

plt.figure()
plt.title('Costs per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Costs', fontsize=18)
plt.plot(costs, 'b-')
# plt.plot(np.mean(accuracies_alternative, axis=1), 'r-')

plt.figure()
plt.title('Accuracy per step', fontsize=18)
plt.xlabel('Steps', fontsize=18)
plt.ylabel('Accuracy', fontsize=18)
plt.plot(random_accuracy, 'b-')

plt.show()