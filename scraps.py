# def pdf(x, mu,sigma):
#     return 1/(sigma * np.sqrt(2 * np.pi)) * np.exp( - (x - mu)**2 / (2 * sigma**2))
#
# def convolved_gaussians(x, mu1,mu2,sigma1,sigma2):
#     sigma = (sigma1**2 + sigma2**2)
#     mu = (mu1 + mu2)/2
#     return 1 / (sigma * np.sqrt(2 * np.pi)) * np.exp(- (x - mu) ** 2 / (2 * sigma ** 2))
#
#
# def kde_func(x, points, bandwidth):
#     data = transformed_data[points]
#     return 1/len(data) * sum([pdf(x,data[i],bandwidth) for i in data])

# model = TSNE(n_components=2, random_state=2016)
# transformed_data = model.fit_transform(X, y=range(100))

#
# fig = plt.figure()
# ax = fig.gca()
# xmin, xmax = min(u[0]), max(u[0])
# ymin, ymax = min(u[1]), max(u[1])
# xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
# positions = np.vstack([xx.ravel(), yy.ravel()])
# kernel = st.gaussian_kde(u, bw_method='silverman')
# f = np.reshape(kernel(positions).T, xx.shape)
#
# ax.set_xlim(xmin, xmax)
# ax.set_ylim(ymin, ymax)
# cfset = ax.contourf(xx, yy, f, cmap='Blues')
#
# ax.plot(u[0],u[1], 'bo')
#
# fig = plt.figure()
# ax = fig.gca()
# xmin, xmax = min(l[0]), max(l[0])
# ymin, ymax = min(l[1]), max(l[1])
# xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
# positions = np.vstack([xx.ravel(), yy.ravel()])
# kernel = st.gaussian_kde(l, bw_method='silverman')
# f = np.reshape(kernel(positions).T, xx.shape)
#
# ax.set_xlim(xmin, xmax)
# ax.set_ylim(ymin, ymax)
# cfset = ax.contourf(xx, yy, f, cmap='Blues')
#
# ax.plot(l[0],l[1], 'ro')

# _, y = load_tractable_data()
# X = pickle.load(open('Tractable_tsne.p','rb'))
# transformed_data = pickle.load(open('TSNE_MNIST.p', 'rb'))
#
# print(len(transformed_data))
# nrows = 5000
# test_size=0.2
#
# L = list(range(100))
# U = list(range(100, int(nrows * (1 - test_size))))

# fig = plt.figure()
# ax = fig.gca()
# ax.set_xlim(xmin, xmax)
# ax.set_ylim(ymin, ymax)
# cfset = ax.contourf(xx, yy, f, cmap='Blues')
# ax.plot(u[0],u[1], 'bo')
# plt.show()

# def draw_sample(X,L,U):
#     d = X.T
#     u = X[U].T
#     l = X[L].T
#     xmin, xmax = min(d[0]), max(d[0])
#     ymin, ymax = min(d[1]), max(d[1])
#     xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
#     positions = np.vstack([xx.ravel(), yy.ravel()])
#     kernel = st.gaussian_kde(u, bw_method='silverman')
#     kernel2 = st.gaussian_kde(l, bw_method='silverman')
#     pdf_grid = (kernel(positions).T - kernel2(positions).T).clip(min=0)
#     f = np.reshape(pdf_grid, xx.shape)
#     index_list = list(range(0,len(pdf)))
#     norm_pdf_grid = [float(i)/sum(pdf) for i in pdf]
#     point_on_grid = [positions[0][np.random.choice(index_list, p=norm_pdf_grid)],
#             positions[1][np.random.choice(index_list, p=norm_pdf_grid)]]
#     nearest_neighbour = KDTree(u.T).query(point_on_grid, k=1)
#     temp = u.T[nearest_neighbour[1]]
#     train_query_index = np.where(np.all(X == temp, axis=1))[0][0]
#     return train_query_index




# x = np.linspace(-5,8,300)
# plt.plot(x, pdf(x,1,1), 'b-')
# plt.plot(x, pdf(x,2,1), 'r-')
# plt.plot(x, convolved_gaussians(x,1,2,1,1), 'g-')
# # plt.plot(x, pdf(x,1,1) - convolved_gaussians(x,1,2,1,1))
# plt.plot(x, [0 if i<0 else i for i in pdf(x,1,1)-convolved_gaussians(x,1,2,1,1)])
# plt.plot(x, [0 if i<0 else i for i in pdf(x,1,1)-pdf(x,2,1)])
# plt.title(r"Gaussians")
# plt.ylabel("Amplitude")
# plt.xlabel("x")
#