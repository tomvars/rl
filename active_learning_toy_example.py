import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
import scipy.sparse
import random
import matplotlib.pyplot as plt
lb = LabelEncoder()
ohe = OneHotEncoder()

data = pd.read_csv('mushroom_data.csv')
data = data.apply(lb.fit_transform)

# print(data)
X = data.drop('class', axis=1)
X= ohe.fit_transform(X)
y = data['class']
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.90, random_state=2016)
y_train = y_train.as_matrix()
X_train = X_train.toarray()
X_test = X_test.toarray()

final_f1 = []
number_of_trials = 100
max_number_of_samples = 30
for trial in range(0, number_of_trials):
    # Uncertainty sampling
    f1_scores, number_of_samples = [], []
    X_train_sample = X_train[:5]
    y_train_sample = y_train[:5]
    X_training = np.delete(X_train, [1,2,3,4,5], 0)
    y_training = np.delete(y_train, [1,2,3,4,5])
    for i in range(0,max_number_of_samples):

        #####################################
        #RANDOM sampling without replacement#
        #####################################
        random_index = random.randint(0, X_training.shape[0]-1)
        X_train_sample = np.vstack((X_train_sample, X_training[random_index]))
        y_train_sample = np.append(y_train_sample, y_training[random_index])
        X_training = np.delete(X_training, random_index,0)
        y_training = np.delete(y_training, random_index)
        #####################################

        svc = svm.SVC(kernel='linear', C=1.0).fit(X_train_sample, y_train_sample)
        y_pred = svc.predict(X_test)
        f1_scores.append(f1_score(y_test, y_pred))
        number_of_samples.append(X_train_sample.shape[0])
    final_f1.append(f1_scores)
plt.figure()
plt.title('F1 score for different number of samples', fontsize=16)
plt.xlabel('Number of samples', fontsize=16)
plt.ylabel('F1 score', fontsize=16)
plt.errorbar(number_of_samples, np.mean(final_f1, axis=0), yerr=np.var(final_f1, axis=0))
plt.show()