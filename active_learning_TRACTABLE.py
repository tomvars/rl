import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn import svm
from sklearn.metrics import f1_score, accuracy_score, precision_score, recall_score, roc_auc_score
from sklearn.metrics.pairwise import rbf_kernel
from sklearn.preprocessing import OneHotEncoder, LabelEncoder
from sklearn.datasets import dump_svmlight_file
import random
import matplotlib.pyplot as plt
from QUIRE_query import query
from bandits import UCB1
import os
from scipy.stats import ttest_ind
import pickle
from data_loader import get_dataset
import itertools
import datetime
import json

def calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred):
    acc = accuracy_score(y_test, y_pred)
    f1 = f1_score(y_test,y_pred)
    pre = precision_score(y_test,y_pred)
    rec = recall_score(y_test,y_pred)
    auc = roc_auc_score(y_test,y_pred)
    final_acc.append(acc)
    final_f1.append(f1)
    final_pre.append(pre)
    final_rec.append(rec)
    final_auc.append(auc)
    return final_f1, final_acc, final_pre, final_rec, final_auc

def test_active_learner(X, y, number_of_trials=1, max_number_of_samples=30, initial_labels=20,
                        method='quire', kernel='rbf', methods=['random', 'uncertain', 'quire', 'difference_picker_2']):
    output_f1, output_acc, output_pre, output_rec, output_auc = [], [], [], [], []
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.80, random_state=2016)
    methods = ['random', 'uncertain', 'difference_picker_2', 'uncertain_entropy', 'quire']
    # Preprocessing for QUIRE
    if method =='ALBL':
        n = len(methods)
        Q = np.ones(n)
        actions = np.arange(n)
        N = np.ones(n)
    if kernel == 'rbf' and (method == 'quire' or method == 'ALBL'):
        lmbda = 1.0
        K = rbf_kernel(X_train)
        L = np.linalg.inv(K + lmbda * np.eye(len(X_train)))

    for trial in range(0, number_of_trials):
        f1_scores, number_of_samples = [0.0], []
        final_f1, final_acc, final_pre, final_rec, final_auc = [], [], [], [], []
        set_of_labels = set(y_train)
        length_of_set_of_labels = len(set_of_labels)
        samples = 0
        Lindex = []
        Uindex = list(range(0,len(y_train)))
        while samples < length_of_set_of_labels:
            sample_index = random.randint(0,len(y_train)-1)
            if y_train[sample_index] in set_of_labels:
                Lindex.append(sample_index)
                Uindex.remove(sample_index)
                samples +=1
                set_of_labels.remove(y_train[sample_index])

        X_labelled = np.array(X_train)[Lindex]
        y_labelled = np.array(y_train)[Lindex]
        X_unlabelled = np.delete(X_train, Lindex, 0)
        y_unlabelled = np.delete(y_train, Lindex, 0)
        print(X_unlabelled.shape)

        t = 0
        for i in range(0, max_number_of_samples):
            t+=1
            svc = svm.SVC(kernel='linear', C=1.0, probability=True,  class_weight='balanced').fit(X_labelled, y_labelled)
            # SELECT A METHOD USING MULTI-ARMED BANDIT SETUP.
            # Monte Carlo Q updates
            if method == 'ALBL':
                accuracy_scores = pickle.load(open('acc_using_random_temp.p', 'rb'))
                arm = methods[UCB1(Q, t, actions, N)]
                print(arm)
                if arm == 'random':
                    q_n_index = methods.index('random')
                    query_index = random.randint(0, X_unlabelled.shape[0] - 1)
                    y_pred = svc.predict(X_test)
                    f1 = accuracy_score(y_test, y_pred)
                    reward = accuracy_scores[i]-f1
                    Q[q_n_index] = Q[q_n_index] + (1.0/N[q_n_index]) * (reward - Q[q_n_index])
                    N[q_n_index]+=1
                    calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0]
                    for i in train_query_index:
                        if i in Uindex:
                            Lindex.append(i)
                            Uindex.remove(i)
                            break

                elif arm == 'uncertain':
                    q_n_index = methods.index('uncertain')
                    probs = svc.predict_proba(X_unlabelled)
                    a = np.mean(np.abs(probs - 1.0 / probs.shape[1]), axis=1)
                    b = np.mean(np.abs(probs - 1.0 / probs.shape[1]), axis=1).min()
                    query_index = random.choice(np.where(a == b)[0])
                    y_pred = svc.predict(X_test)
                    f1 = accuracy_score(y_test, y_pred)
                    reward = accuracy_scores[i]-f1
                    Q[q_n_index] = Q[q_n_index] + (1.0/N[q_n_index]) * (reward - Q[q_n_index])
                    N[q_n_index]+=1
                    calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0]
                    for i in train_query_index:
                        if i in Uindex:
                            Lindex.append(i)
                            Uindex.remove(i)
                            break

                elif arm =='quire':
                    q_n_index = methods.index('quire')
                    query_index = query(y_labelled, K, L, lmbda, Lindex, Uindex)
                    Lindex.append(query_index)
                    Uindex.remove(query_index)
                    y_pred = svc.predict(X_test)
                    f1 = accuracy_score(y_test, y_pred)
                    reward = accuracy_scores[i]-f1
                    Q[q_n_index] = Q[q_n_index] + (1.0/N[q_n_index]) * (reward - Q[q_n_index])
                    N[q_n_index]+=1
                    calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                    point = X_train[query_index]
                    query_index_temp = np.where(np.all(X_unlabelled == point, axis=1))
                    query_index = query_index_temp[0][0]

                elif arm == 'difference_picker_2':
                    q_n_index = methods.index('difference_picker_2')
                    distances = svc.decision_function(X_unlabelled)
                    if len(distances.shape) == 1:
                        query_index = np.argmin(np.abs(distances))
                    else:
                        query_index = np.argmin(np.mean(np.abs(distances), axis=1))
                    y_pred = svc.predict(X_test)
                    f1 = accuracy_score(y_test, y_pred)
                    reward = accuracy_scores[i]-f1
                    Q[q_n_index] = Q[q_n_index] + (1.0 / N[q_n_index]) * (reward - Q[q_n_index])
                    N[q_n_index] += 1
                    calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0]
                    for i in train_query_index:
                        if i in Uindex:
                            Lindex.append(i)
                            Uindex.remove(i)
                            break

                elif arm == 'uncertain_entropy':
                    q_n_index = methods.index('uncertain_entropy')
                    probs = svc.predict_proba(X_unlabelled)
                    entropies = [-(i[0] * np.log(i[0]) + i[1] * np.log(i[1])) for i in probs]
                    b = np.array(entropies).min()
                    query_index = random.choice(np.where(entropies == b)[0])
                    y_pred = svc.predict(X_test)
                    f1 = accuracy_score(y_test, y_pred)
                    reward = accuracy_scores[i] - f1
                    Q[q_n_index] = Q[q_n_index] + (1.0 / N[q_n_index]) * (reward - Q[q_n_index])
                    N[q_n_index] += 1
                    calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                    train_query_index = np.where(np.all(X_train == X_unlabelled[query_index], axis=1))[0]
                    for i in train_query_index:
                        if i in Uindex:
                            Lindex.append(i)
                            Uindex.remove(i)
                            break

            elif method == 'random':
                query_index = random.randint(0, X_unlabelled.shape[0] - 1)
                y_pred = svc.predict(X_test)
                calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
            elif method == 'uncertain':
                probs = svc.predict_proba(X_unlabelled)
                a = np.mean(np.abs(probs -1.0/probs.shape[1]), axis=1)
                b = np.mean(np.abs(probs -1.0/probs.shape[1]), axis=1).min()
                query_index = random.choice(np.where(a == b)[0])
                y_pred = svc.predict(X_test)
                calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
            elif method == 'difference_picker_2':
                distances = svc.decision_function(X_unlabelled)
                if len(distances.shape) == 1:
                    query_index = np.argmin(np.abs(distances))
                else:
                    query_index = np.argmin(np.mean(np.abs(distances),axis=1))
                y_pred = svc.predict(X_test)
                calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
            elif method == 'uncertain_entropy':
                probs = svc.predict_proba(X_unlabelled)
                entropies = [-(i[0]*np.log(i[0]) + i[1]*np.log(i[1])) for i in probs]
                b = np.array(entropies).min()
                query_index = random.choice(np.where(entropies == b)[0])
                y_pred = svc.predict(X_test)
                calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
            elif method == 'quire':
                query_index = query(y_labelled, K, L, lmbda, Lindex, Uindex)
                Lindex.append(query_index)
                Uindex.remove(query_index)
                y_pred = svc.predict(X_test)
                calc_scores(final_f1, final_acc, final_pre, final_rec, final_auc, y_test, y_pred)
                point = X_train[query_index]
                query_index_temp = np.where(np.all(X_unlabelled == point, axis=1))
                query_index = query_index_temp[0][0]
            X_labelled = np.vstack((X_labelled, X_unlabelled[query_index]))
            y_labelled = np.append(y_labelled, y_unlabelled[query_index])
            X_unlabelled = np.delete(X_unlabelled, query_index, 0)
            y_unlabelled = np.delete(y_unlabelled, query_index)
            #####################################

            number_of_samples.append(X_labelled.shape[0])
        output_f1.append(final_f1)
        output_acc.append(final_acc)
        output_pre.append(final_pre)
        output_rec.append(final_rec)
        output_auc.append(final_auc)
    return {'f1':output_f1, 'acc':output_acc, 'pre':output_pre, 'rec':output_rec, 'auc':output_auc}

def main(X,y, dataset, n_labels, path_for_run, nrows):
    max_number_of_samples = n_labels

    progress_filename = path_for_run + '/progress.json'

    # TODO: implement progress.json
    # progress_dict = {'TIME TAKEN': 0, 'Strategies done': 0, 'Strategies to do': 5 * n_experiments}

    # with open(progress_filename, 'w') as fp:
    #     json.dump(progress_dict, fp)

    print('Random...')
    random_output = test_active_learner(X, y, number_of_trials=5,
                                          max_number_of_samples=max_number_of_samples, method='random')
    random = np.mean(random_output['acc'], axis=0)
    pickle.dump(random, open('acc_using_random_temp.p', 'wb'))

    print('ALBL...')
    ALBL_output = test_active_learner(X, y, number_of_trials=5, max_number_of_samples=max_number_of_samples,
                                      method='ALBL')

    print('Uncertain 2...')
    df_picker_output = test_active_learner(X, y, number_of_trials=5, max_number_of_samples=max_number_of_samples,
                                             method='difference_picker_2')

    print('Uncertain...')
    uncertain_output = test_active_learner(X, y, number_of_trials=5,
                                             max_number_of_samples=max_number_of_samples, method='uncertain')
    print('Uncertain entropy...')
    uncertain_entropy_output = test_active_learner(X, y, max_number_of_samples=max_number_of_samples,
                                                     method='uncertain_entropy')
    print('QUIRE...')
    quire_output = test_active_learner(X, y, number_of_trials=1, max_number_of_samples=max_number_of_samples,
                                      method='quire')



    results = [uncertain_output,
    df_picker_output,
    random_output,
    uncertain_entropy_output,
    quire_output,
    ALBL_output]

    # Log files
    json.dump(results, open(path_for_run + '/%s_results.json' % dataset, 'w'))

if __name__ == '__main__':
    # PARAMS
    nrows = [1500]
    ALC_data = ['hiva', 'nova', 'ibn_sina', 'sylva']
    n_labels = [50]
    grid = [ALC_data, n_labels, nrows]
    runs = list(itertools.product(*grid))

    # Document file config in run directory
    PATH = os.getcwd()
    now = datetime.datetime.now()
    datetime_string = now.strftime('%d-%m-%y-%H-%M-%S')
    path_for_run = PATH + '/ALBL_UCB1_logs/' + datetime_string
    config_filename = path_for_run + '/config.json'
    progress_filename = path_for_run + '/progress.json'
    if not os.path.exists(os.path.dirname(config_filename)):
        os.makedirs(os.path.dirname(config_filename))
    metrics = ['f1', 'acc', 'pre', 'rec', 'auc']
    config_dict = {'DATA': ALC_data, 'PATH': path_for_run,
                   'n_labels': n_labels[0], 'n_rows': nrows,
                   'arms':['Random', 'UNC-sm', 'UNC-lc', 'UNC-en', 'Quire','ALBL-UCB'],
                   'metrics':metrics}
    with open(config_filename, 'w') as fp:
        json.dump(config_dict, fp)
    for RUN in runs:
        X, y = get_dataset(RUN[0], nrows=RUN[2])
        main(X, y, RUN[0], RUN[1], path_for_run, RUN[2])

# y = pickle.load(open('Tractable_tsne_python2_labels.p', 'rb'))
# X = pickle.load(open('Tractable_tsne_python2.p', 'rb'))
# ALC_data = ['hiva']
# for name in ALC_data:
#     X, y = get_dataset(name, nrows=500)
#     dump_svmlight_file(X,y, open('libact/examples/%s_svmlight.txt' % name, 'wb'))




