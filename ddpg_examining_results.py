import pickle
import matplotlib.pyplot as plt
import numpy as np
import pickle
import os
import json

# date_time = '21-08-16-18-28-49'
date_time = '22-08-16-10-35-23'
path = os.getcwd()
path_to_logs = path + '/pickle_logs/' + date_time

config_dict = json.load(open(path_to_logs+'/config.json', 'r'))
progress_dict = json.load(open(path_to_logs+'/progress.json', 'r'))
rewards = pickle.load(open(path_to_logs+'/rewards.p', 'rb'))
accuracies = pickle.load(open(path_to_logs+'/accuracies.p', 'rb'))
costs = pickle.load(open(path_to_logs+'/costs.p', 'rb'))
random_accuracy = pickle.load(open('acc_using_random_mnist_pixels.p', 'rb'))
print(progress_dict)
def output_one_key_metric():
    accuracy_list = accuracies[progress_dict['EPISODES DONE']]
    print(accuracy_list)
    sum_of_differences = np.sum(np.array(accuracy_list) - np.array(random_accuracy[:-1]))
    return sum_of_differences

print(config_dict)
print(output_one_key_metric())

plt.figure()
plt.title('Reward per step averaged over episodes')
plt.xlabel('steps')
plt.ylabel('Reward')
plt.plot(np.mean(rewards, axis=0), 'b-', label='random difference')

plt.figure()
plt.title('Reward per Episode averaged over episodes')
plt.xlabel('Episodes')
plt.ylabel('Reward')
plt.plot(np.mean(rewards, axis=1), 'b-', label='random difference')
plt.legend(loc='best')

plt.figure()
plt.title('Final accuracy per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Accuracies', fontsize=18)
plt.plot(np.array(accuracies).T[99], 'b-')

plt.figure()
plt.title('Average accuracy per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Accuracies', fontsize=18)
plt.errorbar(range(np.mean(accuracies, axis=1).size), np.mean(accuracies, axis=1), yerr= np.std(accuracies, axis=1))
# plt.plot(np.mean(accuracies_alternative, axis=1), 'r-')

plt.figure()
plt.title('Costs per episode', fontsize=18)
plt.xlabel('Episode', fontsize=18)
plt.ylabel('Costs', fontsize=18)
plt.plot(costs, 'b-')
# plt.plot(np.mean(accuracies_alternative, axis=1), 'r-')

plt.figure()
plt.title('Accuracy per step', fontsize=18)
plt.xlabel('Steps', fontsize=18)
plt.ylabel('Accuracy', fontsize=18)
plt.plot(random_accuracy, 'b-')

plt.show()