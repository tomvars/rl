import pandas as pd
import os
import numpy as np
from sklearn.manifold import TSNE
import pickle
import json

def load_tractable_data(database=0):
    path_to_file = os.getcwd()

    df1 = pd.read_pickle(path_to_file+'/Tractable_data/conv_features/snapshots/weights_p'+str(database)+'.pdpick')

    df6 = pd.read_csv(path_to_file+'/Tractable_data/conv_features/laird_11_class.csv')

    df6 = df6.rename(columns={'Unnamed: 0': 'name'})

    def format_dataframe(df):
        df['name'] = df.index
        df['name'] = df['name'].map(lambda x: str(x)[2:-1])
        return df

    df1 = format_dataframe(df1)

    df1 = pd.merge(df1, df6)

    # X = np.concatenate((df1,df2,df3,df4,df5), axis=0)
    X = df1.as_matrix()
    split_data = np.hsplit(X, np.array([1024,1036]))
    X, y = split_data[0], split_data[1]
    y = [np.nonzero(i)[0][1] for i in y]
    return X, y

def load_json(name):
    path_X = name+'_features.json'
    path_y = name+'_labels.json'
    X = json.load(open(path_X, 'r'))
    y = json.load(open(path_y, 'r'))
    return np.array(X), y

def load_tractable_json():
    X = json.load(open('Tractable.json', 'r'))
    y = json.load(open('Tractable_labels.json', 'r'))
    return np.array(X),y

def load_mnist_200_features_json():
    X = json.load(open('MNIST_200_features.json', 'r'))
    y = json.load(open('MNIST_200_features_labels.json', 'r'))
    return np.array(X),y