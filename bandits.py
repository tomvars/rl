import pandas as pd
import numpy as np
import os



def UCB1(Q,t,actions,N):
    a_vals = [Q[a]+np.sqrt((2*np.log(t))/N[a]) for a in actions]
    return np.argmax(a_vals)

def EXP4(Q,t,actions,N):
    w=1
    unlabeled_invert_id_idx = 1
    uniform_sampler = 1
    pmin = 1
    K = 1
    query_strategies_ = 1
    delta = 1
    T = 1
    query = np.zeros((N, len(unlabeled_invert_id_idx)))
    if uniform_sampler:
        query[-1, :] = 1. / len(unlabeled_invert_id_idx)
    for i, model in enumerate(query_strategies_):
        query[i][unlabeled_invert_id_idx[model.make_query()]] = 1

    # choice vector, shape = (self.K, )
    W = np.sum(w)
    p = (1 - K * pmin) * w / W + pmin

    # query vector, shape= = (self.n_unlabeled, )
    query_vector = np.dot(p, query)

    reward, ask_id, _ = yield query_vector
    ask_idx = unlabeled_invert_id_idx[ask_id]

    rhat = reward * query[:, ask_idx] / query_vector[ask_idx]

    # The original advice vector in Exp4.P in ALBL is a identity matrix
    yhat = rhat
    vhat = 1 / p
    w = w * np.exp(
        pmin / 2 * (
            yhat + vhat * np.sqrt(
                np.log(N / delta) / K / T
            )
        )
    )
