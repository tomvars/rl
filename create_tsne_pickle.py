from gaussian_kernel_test import load_tractable_data
from sklearn.manifold import TSNE
from sklearn.datasets import dump_svmlight_file
import pickle
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.datasets import load_digits
import numpy as np
from skdata.mnist.views import OfficialImageClassification
from matplotlib import pyplot as plt

# load up data
data = OfficialImageClassification(x_dtype="float32")
print('Loaded data')
x_data = data.all_images
y_data = data.all_labels
plotting = False
# convert image data to float64 matrix. float64 is need for bh_sne
x_data = np.asarray(x_data).astype('float64')
x_data = x_data.reshape((x_data.shape[0], -1))

# For speed of computation, only run on a subset
n = 2000
x_data = x_data[:n]
y_data = y_data[:n]

# perform t-SNE embedding
model = TSNE(n_components=2, random_state=2016)
transformed_data = model.fit_transform(x_data)
# x_data = x_data.reshape(2000,28,28) # NO NEED TO RESHAPE

# data = pd.read_csv('mnist_CNN.csv', nrows=2000)
#
# X = data.drop('class', axis=1)
# y = data['class']
#
# # X = X.as_matrix()
# model = TSNE(n_components=2, random_state=2016)
# transformed_data = model.fit_transform(X)
if plotting:
    vis_x = transformed_data[:, 0]
    vis_y = transformed_data[:, 1]

    plt.scatter(vis_x, vis_y, c=y_data, cmap=plt.cm.get_cmap("jet", 10))
    plt.colorbar(ticks=range(10))
    plt.clim(-0.5, 9.5)
    plt.show()

tsne_dict = {}
for i in range(len(x_data)):
    tsne_dict[str(transformed_data[i])] = x_data[i].tolist()
print(tsne_dict)

with open('MNIST_raw_pixels_labels.json', 'w') as outfile:
    json.dump(np.array(y_data).tolist(), outfile)

with open('MNIST_tsne_dict.json', 'w') as outfile:
    json.dump(tsne_dict, outfile)

with open('MNIST_pixels.json', 'w') as outfile:
    json.dump(x_data.tolist(), outfile)

# X = json.load(open('Tractable.json', 'r'))
# pickle.dump(transformed_data, open('Tractable_tsne_python2.p', 'wb'), protocol=1)
# pickle.dump(y, open('Tractable_tsne_python2_labels.p', 'wb'), protocol=1)
print('done')