import pickle
import matplotlib.pyplot as plt
import numpy as np
import json
import os

PATH = os.getcwd()
ALC_data = ['hiva', 'ibn_sina', 'sylva', 'nova']
dataset = ALC_data[3]

path_for_run = PATH + '/ALBL_logs/26-08-16-16-40-18'
config_dict = json.load(open(path_for_run+'/config.json', 'r'))
results = json.load(open(path_for_run + '/%s_results.json' % dataset, 'r'))
quota = config_dict['n_labels']
result = []
variances = []
# for i in range(5):
#     _temp = []
#     for j in range(i, len(results), 5):
#         _temp.append(results[j])
#         print(results[j])
#     result.append(np.mean(_temp, axis=0))
#     variances.append(np.var(_temp, axis=0))

for i in results:
    result.append(np.mean(i, axis=0))
    variances.append(np.var(i, axis=0))

query_num = np.arange(1, quota + 1)
print(len(query_num))
print(len(result[0]))
# for i, name in enumerate(config_dict['arms']):
#     plt.errorbar(query_num, result[i], yerr=variances[i], label=name)
plt.errorbar(query_num, result[4], yerr=variances[4], label='Uncertain')
plt.xlabel('Number of Queries', fontsize=18)
plt.ylabel('Accuracy', fontsize=18)
plt.title('Accuracy against number of queries for %s' % dataset, fontsize=18)
plt.legend(loc='best')
plt.savefig(path_for_run + '/%s_accuracy.png' % dataset)
plt.close()