import numpy as np
import pandas as pd
from sklearn import svm
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
from mpl_toolkits.mplot3d import Axes3D
from sklearn.cross_validation import train_test_split
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
import random
from sklearn.neighbors.kde import KernelDensity
from sklearn.grid_search import GridSearchCV
from scipy.spatial import KDTree
import scipy.stats as st
from gaussian_kernel_test import load_tractable_data
import pickle

class Data:
    def __init__(self, nrows=2000, test_size=0.2):
        # _, y = load_tractable_data()
        y = pickle.load(open('Tractable_tsne_python2_labels.p', 'rb'))
        self.data = pickle.load(open('Tractable_tsne_python2.p', 'rb'))
        self.n = nrows
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.data,
                                                                                y, test_size=test_size,
                                                                                random_state=2016)
        self.y_train = np.array(self.y_train)
        self.labelled = list(range(5))
        self.unlabelled = list(range(5,int(nrows*(1-test_size))))


    def tsne_transform(self):
        model = TSNE(n_components=2, random_state=2016)
        self.transformed_data = model.fit_transform(self.X_train, y=range(100))
        return self.transformed_data, range(100)


    def display_digit(self, image):
        plt.matshow(image.reshape(28, 28), cmap=plt.cm.gray)
        plt.show()


    def plot2D(self, data):
        H, x_edges, y_edges = np.histogram2d(data.iloc[:,0], data.iloc[:,1], bins=25)
        plt.matshow(H, cmap=plt.cm.gray)
        plt.show()
        return np.vstack([x_edges, y_edges])

    def get_kde_sample(self, U, L):
        params = {'bandwidth': np.logspace(-1, 1, 100)}
        grid = GridSearchCV(KernelDensity(), params)
        grid.fit(U)
        kde1 = grid.best_estimator_

        point_in_continuous_space = kde1.sample(1, random_state=0)
        nearest_neighbour = KDTree(U).query(point_in_continuous_space, k=1)

        temp = U[nearest_neighbour[1][0]]
        train_query_index = np.where(np.all(self.X_train == temp, axis=1))[0][0]
        return train_query_index

    def get_random_tsne_sample(self, data):
        x = random.uniform(min(data[:, 0]), max(data[:, 0]))
        y = random.uniform(min(data[:, 1]), max(data[:, 1]))
        point_in_continuous_space = [x, y]
        nearest_neighbour = KDTree(data).query(point_in_continuous_space, k=1)
        train_query_index = np.where(np.all(self.X_train == data[nearest_neighbour[1]], axis=1))[0][0]
        return train_query_index

    def draw_sample(self):
        d = self.X_train.T
        u = self.X_train[self.unlabelled].T
        l = self.X_train[self.labelled].T
        xmin, xmax = min(d[0]), max(d[0])
        ymin, ymax = min(d[1]), max(d[1])
        xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
        positions = np.vstack([xx.ravel(), yy.ravel()])
        kernel = st.gaussian_kde(u, bw_method='silverman')
        kernel2 = st.gaussian_kde(l, bw_method='silverman')
        pdf_grid = (kernel(positions).T - kernel2(positions).T).clip(min=0)
        index_list = list(range(0, len(pdf_grid)))
        norm_pdf_grid = [float(i) / sum(pdf_grid) for i in pdf_grid]
        point_on_grid = [positions[0][np.random.choice(index_list, p=norm_pdf_grid)],
                         positions[1][np.random.choice(index_list, p=norm_pdf_grid)]]
        nearest_neighbour = KDTree(u.T).query(point_on_grid, k=1)
        temp = u.T[nearest_neighbour[1]]
        train_query_index = np.where(np.all(self.X_train == temp, axis=1))[0][0]
        return train_query_index

def test_active_learner(number_of_trials=2, max_number_of_samples=20, method='random'):
    final_f1 = []
    for j in range(number_of_trials):
        data = Data()
        f1_scores = []
        print('Trial number:', j)
        for i in range(0, max_number_of_samples):
            print('Sample number:', i)
            if method == 'random':
                query_index = random.choice(data.unlabelled)
            elif method == 'tsne_random':
                query_index = data.get_random_tsne_sample(data.transformed_data[data.unlabelled])
            elif method == 'tsne_kde_sample':
                query_index = data.get_kde_sample(data.transformed_data[data.unlabelled], data.transformed_data[data.labelled])
            elif method == 'tsne_modified_kde_sample':
                query_index = data.draw_sample()
            svc = svm.SVC(kernel='linear', C=1.0, probability=True).fit(data.X_train[data.labelled],
                                                                        data.y_train[data.labelled])
            data.labelled.append(query_index)
            data.unlabelled.remove(query_index)
            y_pred = svc.predict(data.X_test)
            f1_scores.append(accuracy_score(data.y_test, y_pred))
            # positions = data.plot2D(data.X_train.iloc[data.labelled])
            # print(data.get_kde_sample(data.transformed_data[data.unlabelled]))
        final_f1.append(f1_scores)
    return final_f1

show = False

if show == True:
    fig = plt.figure()
    # ax = fig.add_subplot(111, projection='3d')
    # ax.scatter(x,y,z)
    plt.show()
max_number_of_samples = 200
number_of_trials = 6
if __name__ == "__main__" :
    data = pickle.load(open('Tractable_tsne_python2.p', 'rb'))
    plt.figure()
    plt.plot(data.T[0],-data.T[1], 'bo')
    plt.figure()
    H, _, _ = np.histogram2d(data.T[0], data.T[1], bins = 64)
    plt.imshow(H.T, cmap=plt.get_cmap('gray_r'))
    plt.show()
    # print('TSNE trials...')
    # tsne_modified_f1 = test_active_learner(number_of_trials=number_of_trials, max_number_of_samples=max_number_of_samples, method='tsne_modified_kde_sample')
    # print('Random trials...')
    # random_f1 = test_active_learner(number_of_trials=number_of_trials, max_number_of_samples=max_number_of_samples, method='random')
    # # tsne_f1 = test_active_learner(number_of_trials=1, max_number_of_samples=max_number_of_samples, method='tsne_kde_sample')
    # # random_tsne_f1 = test_active_learner(number_of_trials=1, max_number_of_samples=max_number_of_samples, method='tsne_random')
    # plt.figure()
    # plt.title('F1 score for different number of samples', fontsize=16)
    # plt.xlabel('Number of samples', fontsize=16)
    # plt.ylabel('Accuracy', fontsize=16)
    # plt.errorbar(range(5, max_number_of_samples + 5), np.mean(random_f1, axis=0),
    #              yerr=np.var(random_f1, axis=0), label='RANDOM')
    # plt.errorbar(range(5, max_number_of_samples + 5), np.mean(tsne_modified_f1, axis=0),
    #              yerr=np.var(tsne_modified_f1, axis=0), label='TSNE/modified KDE/1NN')
    # # plt.errorbar(range(5, max_number_of_samples + 5), np.mean(tsne_f1, axis=0),
    # #              yerr=np.var(tsne_f1, axis=0), label='TSNE/KDE/1NN')
    # # plt.errorbar(range(5, max_number_of_samples + 5), np.mean(random_tsne_f1, axis=0),
    # #              yerr=np.var(random_tsne_f1, axis=0), label='RANDOM IN TSNE')
    # plt.legend(loc='lower right')
    # filename = 'tsne_active_learning' + str(number_of_trials) + 'trials_' + str(max_number_of_samples) + 'samples.png'
    # plt.savefig(filename)
