import gym
from ddpg import *
import time
import matplotlib.pyplot as plt
import json
from pprint import pprint

with open('/tmp/ActiveLearning-v0-DDPG-experiment/openaigym.episode_batch.0.20755.stats.json') as data_file:
    data = json.load(data_file)

plt.figure()
plt.title('Reward against time')
plt.xlabel('Episode')
plt.ylabel('Reward')
plt.plot(data['episode_rewards'])
plt.show()
