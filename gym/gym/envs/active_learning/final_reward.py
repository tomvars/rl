"""
TSNE active learning method environment. Need to implement:
        __init__
        step
        reset
        render
        close
        configure
        seed

"""
from sklearn.metrics import accuracy_score
from sklearn.linear_model import SGDClassifier
from sklearn import svm
from gaussian_kernel_test import load_tractable_data
from sklearn.cross_validation import train_test_split
import logging
import math
import gym
from gym import spaces
from gym.utils import seeding
import numpy as np
import pickle
import matplotlib.pyplot as plt
from scipy.spatial import KDTree
from gaussian_kernel_test import load_tractable_json
from gaussian_kernel_test import load_json
import json
import random

logger = logging.getLogger(__name__)
STEPS = 200
BINS = 28


class ActiveLearningEnvFINAL(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 50
    }

    def __init__(self):
        self.ENV_CONFIG = json.load(open('ENV_CONFIG.json'))

        self.bins = BINS

        # self.x, self.y = load_tractable_json()
        self.x, self.y = load_json('MNIST_raw_pixels_tsne')

        self.random_accuracies = pickle.load(open('acc_using_random_mnist_pixels.p', 'rb'))
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.x, self.y,
                                                                                test_size=0.50, random_state=2016)
        self.mean_state = self.get_mean_through_random_permutations()
        self.labelled = np.array(list(range(5)))
        self.unlabelled = list(range(5, len(self.X_train) - 1))
        self.unlabelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.unlabelled],
                                                     self.X_train.T[1][self.unlabelled], bins=BINS)
        self.labelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.labelled],
                                                   self.X_train.T[1][self.labelled], bins=BINS)

        self.action_space = spaces.Box(min(self.x.T[0]), max(self.x.T[0]), (2,))
        self.observation_space = spaces.Box(min(self.unlabelled_state.flatten()), max(self.unlabelled_state.flatten()),
                                            shape=(BINS, BINS, 2))
        self.rewards = [0]
        self.tsne_dict = json.load(open('MNIST_tsne_dict.json', 'r'))

        self._seed()
        self.reset()
        self.viewer = None
        self.steps_beyond_done = None
        self.steps = 0
        self.state = np.array([self.unlabelled_state, self.labelled_state]).T - self.mean_state

        # Just need to initialize the relevant attributes
        self._configure()

    def _configure(self, display=None):
        self.display = display

    def _seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def _step(self, action):
        assert self.action_space.contains(action), "%r (%s) invalid" % (action, type(action))
        self.steps += 1
        # Update state given new action
        point_index = self.get_nearest_neighbour(action)
        done = False

        reward = 1  # Calculate the reward based on the state and action.
        # TIP: use logger.warn('something')
        self.labelled = [int(i) for i in np.append(int(point_index), self.labelled)]
        self.unlabelled.remove(point_index)
        # svc = svm.SVC(kernel='linear', C=1.0, probability=True).fit(self.X_train[self.labelled],
        #                                                             np.array(self.y_train)[self.labelled])

        # IF USING TSNE
        # svc = SGDClassifier(warm_start=True).fit(self.X_train[self.labelled],
        #                                                             np.array(self.y_train)[self.labelled])
        # y_pred = svc.predict(self.X_test)

        # IF USING RAW PIXELS
        raw_pixel_X_train = [self.tsne_dict[str(i)] for i in self.X_train[self.labelled]]
        svc = SGDClassifier(warm_start=True).fit(raw_pixel_X_train,
                                                 np.array(self.y_train)[self.labelled])
        raw_pixel_X_test = [self.tsne_dict[str(i)] for i in self.X_test]
        y_pred = svc.predict(raw_pixel_X_test)

        accuracy = accuracy_score(self.y_test, y_pred)
        # reward = accuracy - self.rewards[-1] # change in accuracy
        # reward = accuracy - self.random_accuracies[self.steps]  # Comparing to RANDOM
        if self.steps<STEPS:
            reward = 0
        else:
            reward = accuracy
        self.rewards.append(reward)

        self.unlabelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.unlabelled],
                                                     self.X_train.T[1][self.unlabelled], bins=BINS, normed=True)

        self.labelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.labelled],
                                                   self.X_train.T[1][self.labelled], bins=BINS, normed=True)
        self.state = np.array([self.unlabelled_state, self.labelled_state]).T - self.mean_state
        return self.state, reward, done, {'acc': accuracy}

    def _reset(self):
        self.steps = 0
        self.rewards = [0]
        self.labelled = np.array(list(range(5)))
        self.unlabelled = list(range(5, len(self.X_train) - 1))
        self.unlabelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.unlabelled],
                                                     self.X_train.T[1][self.unlabelled], bins=BINS, normed=True)

        self.labelled_state, _, _ = np.histogram2d(self.X_train.T[0][self.labelled],
                                                   self.X_train.T[1][self.labelled], bins=BINS, normed=True)

        self.state = np.array(
            [self.unlabelled_state, self.labelled_state]).T - self.mean_state  # original all unlabelled state
        return self.state

    def _render(self, mode='points', close=False):
        if close:
            if self.viewer is not None:
                self.viewer.close()
                self.viewer = None
            return

        if self.viewer is None and mode == 'points':
            # plt.hold(True)
            # unlabelled_data = self.x[self.unlabelled]
            # labelled_data = self.x[self.labelled]
            # self.ax1.scatter(unlabelled_data.T[0], unlabelled_data.T[1])
            # self.ax1.scatter(labelled_data.T[0], labelled_data.T[1], c='r')
            # plt.draw()
            pass
        elif self.viewer is None and mode == 'reward':
            pass

        return None

    def get_nearest_neighbour(self, point_in_continuous_space):
        U = self.X_train[self.unlabelled]
        try:
            nearest_neighbour = KDTree(U).query(point_in_continuous_space, k=1)
            temp = U[nearest_neighbour[1]]
            train_query_index = np.where(np.all(self.X_train == temp, axis=1))[0][0]
        except:
            raise ReferenceError
        return train_query_index

    def get_mean_through_random_permutations(self):
        X_train = self.X_train
        number_of_samples = 1000
        unlabelled_states = []
        labelled_states = []
        for i in range(number_of_samples):
            all = list(range(len(X_train) - 1))
            labelled = np.random.choice(all, random.randint(0, 200), replace=False)
            unlabelled = np.delete(all, labelled)
            assert (len(labelled) + len(unlabelled) == len(X_train) - 1), \
                '%s + %s is not %s' % (len(labelled), len(unlabelled), len(X_train) - 1)
            unlabelled_state, _, _ = np.histogram2d(X_train.T[0][unlabelled],
                                                    X_train.T[1][unlabelled], bins=BINS)
            labelled_state, _, _ = np.histogram2d(X_train.T[0][labelled],
                                                  X_train.T[1][labelled], bins=BINS)
            unlabelled_states.append(unlabelled_state)
            labelled_states.append(labelled_state)
        return np.array([np.mean(unlabelled_states, axis=0), np.mean(labelled_states, axis=0)]).T
